import { OBJECT_FACTORY, TEXTURE_SIZE, TEXTURE_OFFSET_LEFT, TEXTURE_OFFSET_TOP, GAME_TYPES } from './constants';
import Component from '../ts/engine/Component';
import Scene from '../ts/engine/Scene';
import Msg from '../ts/engine/Msg';
import { GameObject } from './gameObject';
import PIXIObjectBuilder from '../ts/engine/PIXIObjectBuilder';
import { PIXICmp } from '../ts/engine/PIXIObject';

export class ObjectFactoryComponent extends Component {
    private game_map: Array<Array<GameObject>> = null;
    private bomb_map: Array<Array<GameObject>> = null;

    constructor(scene: Scene, game_map: Array<Array<GameObject>>, bomb_map: Array<Array<GameObject>>) {
        super();
        this.scene = scene;
        this.game_map = game_map;
        this.bomb_map = bomb_map;

        this.subscribe(OBJECT_FACTORY.CREATE);
        this.subscribe(OBJECT_FACTORY.DESTROY);
        this.subscribe(OBJECT_FACTORY.REPLACE);
    }

    onMessage(msg: Msg) {
        if (msg.action === OBJECT_FACTORY.CREATE) {
            this.createObject(msg.data);
        }
        if (msg.action === OBJECT_FACTORY.DESTROY) {
            this.destroyObject(msg.data);
        }
        if (msg.action === OBJECT_FACTORY.REPLACE) {
            this.replaceObject(msg.data);
        }
    }

    onUpdate(delta: number, absolute: number) { 

    }
    
    private createObject(data: any) {
        let objectType = data.type;
        let posX = data.position.posX;
        let posY = data.position.posY;
        let gameLevel = data.gameLevel;
        let swap = data.swap;
        let textureTag = data.textureTag;
        let ownerPixiObj = data.ownerObj;
        
        let texture = PIXI.Texture.fromImage(textureTag);

        let OBJ = new PIXICmp.Sprite(textureTag, texture);
        if (objectType != GAME_TYPES.BOMB_TYPE) {
            this.game_map[posX][posY] = new GameObject(posX, posY, true, OBJ, objectType, gameLevel); 
        } else {
            this.bomb_map[posX][posY] = new GameObject(posX, posY, true, OBJ, objectType, gameLevel); 
        }   

        let absoluteX = posX*TEXTURE_SIZE + TEXTURE_OFFSET_LEFT;
        let absoluteY = posY*TEXTURE_SIZE + TEXTURE_OFFSET_TOP;

        new PIXIObjectBuilder(this.scene).globalPos(absoluteX, absoluteY).anchor(0.5, 0.5).build(OBJ);
        this.scene.stage.getPixiObj().addChild(OBJ); 

        // Swap children to KEEP PLAYER on TOP (z order) // Little magic
        if ( swap ) {
            this.scene.stage.getPixiObj().swapChildren(OBJ, ownerPixiObj);
        }
    }

    private destroyObject(data: any) { 

    }

    private replaceObject(data: any) { 
        let objectType = data.type;
        let textureTag = data.textureTag;
        let posX = data.position.posX;
        let posY = data.position.posY;

        let texture = PIXI.Texture.fromImage(textureTag);
        
        this.game_map[posX][posY].getPixiSprite().texture = texture;
        this.game_map[posX][posY].setGameType(objectType);

        if (objectType === GAME_TYPES.EXIT_TYPE) {
            // send animation message
        }
    }

}