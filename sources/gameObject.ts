import { PIXICmp } from '../ts/engine/PIXIObject';

export class GameObject {
    private posX: number;
    private posY: number;
    private visible: boolean = true; 
    private pixiSprite: PIXICmp.Sprite = null;
    private gameType: string = null;
    private created: number = null;
    private score: number = null;
    private data: any = [];

    constructor(
        posX: number,
        posY: number,
        visible: boolean,
        pixiSprite: PIXICmp.Sprite,
        gameType: string,
        gameLevel: number = 0,
        score: number = 0
    ) {
        this.posX = posX,
        this.posY = posY,
        this.visible = visible;
        this.pixiSprite = pixiSprite;
        this.gameType = gameType;
        this.score = score;
    }

    getPosX(): number {
        return this.posX;
    }

    setPosX(posX: number) {
        this.posX = posX;
    }

    getPosY(): number {
        return this.posY;
    }

    setPosY(posY: number) {
        this.posY = posY;
    }

    getGameType(): string {
        return this.gameType;
    }

    setGameType(gameType: string) {
        this.gameType = gameType;
    }

    getPixiSprite(): PIXICmp.Sprite {
        return this.pixiSprite;
    }

    getCreated(): number {
        return this.created;
    }

    setCreated(created: number) {
        this.created = created;
    }

    getScore(): number {
        return this.score;
    }

    setScore(score: number) {
        this.score = score;
    }

    getData(key: string = null): any {
        if (key) 
            return this.data[key]
        return this.data;
    }

    setData(key: string, value: any) {
        this.data[key] = value;
        return this;
    }
}