import DebugComponent from '../ts/components/DebugComponent';
import { PixiRunner } from '../ts/PixiRunner'
import { PLAYER_TEXTURES, EXPLOSION_TEXTURES, ANIMATION_EXIT, ANIMATION_MONSTERS, ANIMATION_BOMB_POWERUP, ANIMATION_BLAST_POWERUP, SOUNDS } from './constants';
import { GAME_LEVELS, GAME_WIDTH, TEXTURE_BACKGROUND, TEXTURE_BRICK, TEXTURE_WALL, TEXTURE_TABLE, TEXTURE_BOMB } from './constants';
import { TEXTURE_TABLE_DOWN, TEXTURE_TABLE_UP, TEXTURE_TABLE_LEFT, TEXTURE_TABLE_RIGHT, TEXTURE_TABLE_CORNER_LEFT, TEXTURE_TABLE_CORNER_RIGHT } from './constants';
import { GameObject } from './gameObject';
import PIXIObjectBuilder from '../ts/engine/PIXIObjectBuilder';
import { PIXICmp } from '../ts/engine/PIXIObject';
import { Game } from './game';
import { Player } from './player';
import { Monsters } from './monsters';

class Components {
    engine: PixiRunner;
    debug: DebugComponent;

    GAME_LEVEL: number = 1;   
    GAME_TIER: number = 1;
    PLAYERS: number = 1;

    private game_map: Array<Array<GameObject>> = null;
    private bomb_map: Array<Array<GameObject>> = null;
    private monster_map: Array<Array<GameObject>> = null;

    constructor() {
        this.engine = new PixiRunner();
        this.engine.init(document.getElementById("gameCanvas") as HTMLCanvasElement, 1);
        PIXI.loader.reset();

        let table = TEXTURE_TABLE;
        PIXI.loader.add(table, 'static/img/level/' + table + '.png');
        for (let i = 1; i <= GAME_LEVELS; i++) {
            // Load Border textures
            let tableUp = TEXTURE_TABLE_UP + i;
            PIXI.loader.add(tableUp, 'static/img/level/' + tableUp + '.png');
            let tableLeft = TEXTURE_TABLE_LEFT + i;
            PIXI.loader.add(tableLeft, 'static/img/level/' + tableLeft + '.png');
            let tableRight = TEXTURE_TABLE_RIGHT + i;
            PIXI.loader.add(tableRight, 'static/img/level/' + tableRight + '.png');
            let tableDown = TEXTURE_TABLE_DOWN + i;
            PIXI.loader.add(tableDown, 'static/img/level/' + tableDown + '.png');
            let tableCornerL = TEXTURE_TABLE_CORNER_LEFT + i;
            PIXI.loader.add(tableCornerL, 'static/img/level/' + tableCornerL + '.png');
            let tableCornerR = TEXTURE_TABLE_CORNER_RIGHT + i;
            PIXI.loader.add(tableCornerR, 'static/img/level/' + tableCornerR + '.png');

            // Load Background textures
            let background = TEXTURE_BACKGROUND + i;
            PIXI.loader.add(background, 'static/img/bricks/' + background + '.png');

            // Load Background destroy textures
            for (let j = 1; j <= 4; j++) {
                let bgr = TEXTURE_BACKGROUND + i + j;
                PIXI.loader.add(bgr, 'static/img/bricks/' + bgr + '.png');
            }

            // Load Brick textures
            let brick = TEXTURE_BRICK + i;
            PIXI.loader.add(brick, 'static/img/bricks/' + brick + '.png');

            // Load Wall textures
            let wall = TEXTURE_WALL + i;
            PIXI.loader.add(wall, 'static/img/bricks/' + wall + '.png');
        }

        // Load Monster textures
        for (let i = 1; i <= Object.keys(ANIMATION_MONSTERS).length; i++) { 
            for (let j = 0; j < ANIMATION_MONSTERS[i].live.length; j++) {
                let monster = ANIMATION_MONSTERS[i].live[j];
                PIXI.loader.add(monster, 'static/img/monsters/' + monster + '.png');
            }
            for (let j = 0; j < ANIMATION_MONSTERS[i].dead.length; j++) {
                let monster = ANIMATION_MONSTERS[i].dead[j];
                PIXI.loader.add(monster, 'static/img/monsters/' + monster + '.png');
            }
        }

        // Load Player textures
        for (let i = 1; i <= 3; i++) { 
            let playerF = PLAYER_TEXTURES.FORW_TEXTURE + i;
            PIXI.loader.add(playerF, 'static/img/player/' + playerF + '.png');
            let playerL = PLAYER_TEXTURES.LEFT_TEXTURE + i;
            PIXI.loader.add(playerL, 'static/img/player/' + playerL + '.png');
            let playerR = PLAYER_TEXTURES.RIGHT_TEXTURE + i;
            PIXI.loader.add(playerR, 'static/img/player/' + playerR + '.png');
            let playerB = PLAYER_TEXTURES.BACK_TEXTURE + i;
            PIXI.loader.add(playerB, 'static/img/player/' + playerB + '.png');
        }

        // Load Bomb textures
        for (let i = 1; i <= 3; i++) { 
            let bomb = TEXTURE_BOMB + i;
            PIXI.loader.add(bomb, 'static/img/' + bomb + '.png');
        }

        // Load Exit textures
        for (let i = 1; i <= ANIMATION_EXIT.length; i++) { 
            let exit = ANIMATION_EXIT[i-1];
            PIXI.loader.add(exit, 'static/img/' + exit + '.png');
        }

        // Load Bomb PowerUp textures
        for (let i = 1; i <= ANIMATION_BOMB_POWERUP.length; i++) { 
            let pup = ANIMATION_BOMB_POWERUP[i-1];
            PIXI.loader.add(pup, 'static/img/' + pup + '.png');
        }

        // Load Blast PowerUp textures
        for (let i = 1; i <= ANIMATION_BLAST_POWERUP.length; i++) { 
            let pup = ANIMATION_BLAST_POWERUP[i-1];
            PIXI.loader.add(pup, 'static/img/' + pup + '.png');
        }

        // Load Explosion textures
        for (let i = 1; i <= 4; i++) {
            let exC = EXPLOSION_TEXTURES.CENTER_TEXTURE + i;
            PIXI.loader.add('explosion_' + exC, 'static/img/explosion/' + exC + '.png');
            let exB = EXPLOSION_TEXTURES.BOTTOM_TEXTURE + i;
            PIXI.loader.add('explosion_' + exB, 'static/img/explosion/' + exB + '.png');
            let exL = EXPLOSION_TEXTURES.LEFT_TEXTURE + i;
            PIXI.loader.add('explosion_' + exL, 'static/img/explosion/' + exL + '.png');
            let exR = EXPLOSION_TEXTURES.RIGHT_TEXTURE + i;
            PIXI.loader.add('explosion_' + exR, 'static/img/explosion/' + exR + '.png');
            let exT = EXPLOSION_TEXTURES.TOP_TEXTURE + i;
            PIXI.loader.add('explosion_' + exT, 'static/img/explosion/' + exT + '.png');
        }

        // Menu textures
        PIXI.loader.add('menu_1', 'static/img/start_game.png');
        PIXI.loader.add('menu_2', 'static/img/level.png');
        PIXI.loader.add('menu_3', 'static/img/end.png'); 
        PIXI.loader.add('menu_4', 'static/img/end_loose.png');
        PIXI.loader.add('circle_green', 'static/img/circle_green.png');
        PIXI.loader.add('circle_red', 'static/img/circle_red.png');

        // Sounds
        PIXI.sound.add(SOUNDS.WIN, 'static/music/Stage_Clear.mp3');
        PIXI.sound.add(SOUNDS.MENU, 'static/music/Stage_Music_1.mp3');
        PIXI.sound.add(SOUNDS.GAME, 'static/music/Stage_Music_2.mp3');
        PIXI.sound.add(SOUNDS.BOMB, 'static/music/bomb.mp3');
        PIXI.sound.add(SOUNDS.POWERUP, 'static/music/bonus.mp3');
        PIXI.sound.add(SOUNDS.DIE, 'static/music/Death.mp3');
        PIXI.sound.add(SOUNDS.LOOSE, 'static/music/Dead.mp3');
        

        PIXI.loader.load(() => this.onAssetsLoaded());
    }

    onAssetsLoaded() {
        this.initGameMap();
        this.initBombMap();
        this.initMonsterMap();

        // Draw menu picker
        this.drawMenu();

        /*this.engine.scene.addGlobalAttribute("players", this.PLAYERS);
        this.engine.scene.addGlobalAttribute("level", this.GAME_LEVEL);

        new Game(this.game_map, this.bomb_map).init(this.engine.scene);
        new Monsters(this.game_map, this.bomb_map, this.monster_map).init(this.engine.scene);
        for (let i = 1; i <= this.PLAYERS; i++) {
            new Player(this.game_map, this.bomb_map, this.monster_map, i).init(this.engine.scene);
        }
                
        console.log(this.game_map);*/
    }

    private drawGame() {
        this.engine.scene.addGlobalAttribute("players", this.PLAYERS);
        this.engine.scene.addGlobalAttribute("level", this.GAME_LEVEL);
        
        new Game(this.game_map, this.bomb_map).init(this.engine.scene);
        new Monsters(this.game_map, this.bomb_map, this.monster_map).init(this.engine.scene);
        for (let i = 1; i <= this.PLAYERS; i++) {
            new Player(this.game_map, this.bomb_map, this.monster_map, i).init(this.engine.scene);
        }
    }

    private drawMenu() {
        PIXI.sound.play(SOUNDS.MENU);

        // Texture 1/2 Players picker
        let texture = PIXI.Texture.fromImage("menu_1");
        let menukObj = new PIXICmp.Sprite("menu_1", texture);
        menukObj.height = 825;
        menukObj.width = 750;
        new PIXIObjectBuilder(this.engine.scene).globalPos(0, 0).build(menukObj);
        this.engine.scene.stage.getPixiObj().addChild(menukObj);

        // P1
        this.setcircleAroundP1();
        // P2
        this.setcircleAroundP2(); 
    }

    private drawLevelMenu() {
        let circles = this.engine.scene.findAllObjectsByTag("circle_green");
        let menuObj = this.engine.scene.findFirstObjectByTag("menu_1");
        let texture = PIXI.Texture.fromImage("menu_2");
        
        let that = this;

        circles[0].getPixiObj().on('pointerdown', function() { 
            that.GAME_LEVEL = 1;
            circles.forEach(c => {
                c.getPixiObj().destroy();
            });
            menuObj.getPixiObj().destroy();
            that.drawGame();
        });

        circles[1].getPixiObj().on('pointerdown', function() { 
            that.GAME_LEVEL = 2;
            circles.forEach(c => {
                c.getPixiObj().destroy();
            });
            menuObj.getPixiObj().destroy();
            that.drawGame();
        });
        
        let pixi = <PIXICmp.Sprite> menuObj.getPixiObj();
        pixi.texture = texture;
    }

    private setcircleAroundP1() {
        let circleTexture = PIXI.Texture.fromImage("circle_green");
        let circleObjP1 = new PIXICmp.Sprite("circle_green", circleTexture);
        circleObjP1.height = 175;
        circleObjP1.width = 175;
        new PIXIObjectBuilder(this.engine.scene).globalPos(300, 610).build(circleObjP1);
        this.engine.scene.stage.getPixiObj().addChild(circleObjP1);

        let that = this;

        circleObjP1.interactive = true;
        circleObjP1.buttonMode = true;
        circleObjP1.on('mouseover', function() {
            let circleTexture = PIXI.Texture.fromImage("circle_red");
            circleObjP1.texture = circleTexture;
        });
        circleObjP1.on('mouseout', function() { 
            let circleTexture = PIXI.Texture.fromImage("circle_green");
            circleObjP1.texture = circleTexture;
        });
        circleObjP1.on('pointerdown', function() { 
            that.PLAYERS = 1;
            that.drawLevelMenu();
        });
    }

    private setcircleAroundP2() {
        let circleTexture = PIXI.Texture.fromImage("circle_green");
        let circleObjP2 = new PIXICmp.Sprite("circle_green", circleTexture);
        circleObjP2.height = 175;
        circleObjP2.width = 175;
        new PIXIObjectBuilder(this.engine.scene).globalPos(650, 610).build(circleObjP2);
        this.engine.scene.stage.getPixiObj().addChild(circleObjP2);

        let that = this;

        circleObjP2.interactive = true;
        circleObjP2.buttonMode = true;
        circleObjP2.on('mouseover', function() {
            let circleTexture = PIXI.Texture.fromImage("circle_red");
            circleObjP2.texture = circleTexture;
        });
        circleObjP2.on('mouseout', function() { 
            let circleTexture = PIXI.Texture.fromImage("circle_green");
            circleObjP2.texture = circleTexture;
        });
        circleObjP2.on('pointerdown', function() { 
            that.PLAYERS = 2;
            console.log(that.PLAYERS);
            that.drawLevelMenu();
        });
    }

    private initGameMap() {
        this.game_map = [];
        for (let i = 0; i < GAME_WIDTH; i++) 
            this.game_map[i] = []; 
    }

    private initBombMap() {
        this.bomb_map = [];
        for (let i = 0; i < GAME_WIDTH; i++) 
            this.bomb_map[i] = []; 
    }

    private initMonsterMap() {
        this.monster_map = [];
        for (let i = 0; i < GAME_WIDTH; i++) 
            this.monster_map[i] = [];
    }
}

new Components();

