import { PLAYER_STATS, PLAYER_1_INPUTS, SOUNDS } from './constants';
import Component from '../ts/engine/Component';
import Scene from '../ts/engine/Scene';
import Msg from '../ts/engine/Msg';
import PIXIObjectBuilder from '../ts/engine/PIXIObjectBuilder';
import { PIXICmp } from '../ts/engine/PIXIObject';

export class ScoreComponent extends Component {
    PLAYERS: number = null;

    PLAYER_1_LIFES: number = 2;
    PLAYER_2_LIFES: number = 2;
    PLAYERS_SCORE: number = 0;
    MAX_SCORE: number = 0;

    constructor(scene: Scene) { 
        super();
        this.scene = scene;
        this.PLAYERS = scene.getGlobalAttribute("players");

        this.subscribe(PLAYER_STATS.LIFES_MINUS);
        this.subscribe(PLAYER_STATS.LIFES_PLUS);
        this.subscribe(PLAYER_1_INPUTS.PAUSE);
        this.subscribe(PLAYER_STATS.SCORE_PLUS);
    }

    onMessage(msg: Msg) {
         
        if (msg.action === PLAYER_STATS.LIFES_MINUS) {
            if (msg.data.playerId == 1) {
                this.PLAYER_1_LIFES --;
            }
            if (msg.data.playerId == 2) {
                this.PLAYER_2_LIFES --;
            }

            if (this.PLAYER_1_LIFES == 0 || this.PLAYER_2_LIFES == 0) {
                this.endGame();
                return;
            }

            this.updateLifes();
        }
        if (msg.action === PLAYER_STATS.LIFES_PLUS) {
            if (msg.data.playerId == 1) {
                this.PLAYER_1_LIFES ++;
            }
            if (msg.data.playerId == 2) {
                this.PLAYER_2_LIFES ++;
            }
            this.updateLifes();
        }
        if (msg.action === PLAYER_STATS.SCORE_PLUS) {
            this.updateScore(msg.data.score);
        }
    }
    
    onInit() {
        let textObj = new PIXICmp.Text('stats_score', '0');
        new PIXIObjectBuilder(this.scene).globalPos(50, 23).build(textObj);
        this.scene.stage.getPixiObj().addChild(textObj);

        textObj = new PIXICmp.Text('stats_lifesP1', this.PLAYER_1_LIFES.toString());
        new PIXIObjectBuilder(this.scene).globalPos(380, 23).build(textObj);
        this.scene.stage.getPixiObj().addChild(textObj);

        if (this.PLAYERS == 2) { 
            textObj = new PIXICmp.Text('stats_lifesP2',  this.PLAYER_2_LIFES.toString());
            new PIXIObjectBuilder(this.scene).globalPos(454, 23).build(textObj);
            this.scene.stage.getPixiObj().addChild(textObj);
        }    

        textObj = new PIXICmp.Text('stats_hight_score', '2 367 800');
        new PIXIObjectBuilder(this.scene).globalPos(520, 23).build(textObj);
        this.scene.stage.getPixiObj().addChild(textObj);
    }

    private updateScore(score: number) {
        this.PLAYERS_SCORE += score;
        
        let scoreObj = <PIXICmp.Text> this.scene.findFirstObjectByTag('stats_score');
        scoreObj.setText(this.PLAYERS_SCORE.toString());
    }

    private updateLifes() {
        let scoreObj = <PIXICmp.Text> this.scene.findFirstObjectByTag('stats_lifesP1');
        scoreObj.setText(this.PLAYER_1_LIFES.toString());

        if (this.PLAYERS == 2) { 
            scoreObj = <PIXICmp.Text> this.scene.findFirstObjectByTag('stats_lifesP2');
            scoreObj.setText(this.PLAYER_2_LIFES.toString()); 
        }
    }

    private endGame() {
        this.sendMessage(SOUNDS.LOOSE); // -> soundComponent
        this.scene.clearScene();
        let texture = PIXI.Texture.fromImage("menu_4");
        let menukObj = new PIXICmp.Sprite("menu_4", texture);
        menukObj.height = 825;
        menukObj.width = 750;
        new PIXIObjectBuilder(this.scene).globalPos(0, 0).build(menukObj);
        this.scene.stage.getPixiObj().addChild(menukObj);
    }
}