import { GAME_WIDTH, GAME_HEIGHT, TEXTURE_OFFSET_LEFT, PLAYER_1_INPUTS, PLAYER_2_INPUTS, GAME_TYPES, TEXTURE_OFFSET_TOP, GAME_MAPS, SOUNDS, PLAYER_COLLISIONS, PLAYER_STATS } from './constants';
import { TEXTURE_SIZE } from './constants';
import Component from '../ts/engine/Component';
import Scene from '../ts/engine/Scene';
import { GameObject } from './gameObject';
import Msg from '../ts/engine/Msg';
import Queue from '../ts/utils/datastruct/Queue';
import { isNullOrUndefined } from 'util';

class Node {
    position = { "posX": 0, "posY": 0 };
    distance: number;
    parent: Node = null; 

    constructor(position: any, distance: number, parent: Node = null) {
        this.position = position;
        this.distance = distance;
        this.parent = parent;
    }
}

export class MonsterMoveComponent extends Component {
    MOVE_SPEED: number = 0.055;
    ALLOWED_OFFSET: number = 2;
    UPDATE_SPEED: number = 1000; //ms

    PLAYERS: number = null;
    GAME_LEVEL: number = null;

    private game_map: Array<Array<GameObject>> = null;
    private bomb_map: Array<Array<GameObject>> = null;
    private monster_map: Array<Array<GameObject>> = null;
    private playersPositions = [];

    constructor(scene: Scene, game_map: Array<Array<GameObject>>, bomb_map: Array<Array<GameObject>>, monster_map: Array<Array<GameObject>>) {
        super();
        this.scene = scene;
        this.game_map = game_map;
        this.bomb_map = bomb_map;
        this.monster_map = monster_map;
        this.PLAYERS = scene.getGlobalAttribute("players");
        this.GAME_LEVEL = scene.getGlobalAttribute("level");

        this.subscribe(PLAYER_1_INPUTS.MOVING_UP);
        this.subscribe(PLAYER_1_INPUTS.MOVING_LEFT);
        this.subscribe(PLAYER_1_INPUTS.MOVING_RIGHT);
        this.subscribe(PLAYER_1_INPUTS.MOVING_DOWN);
        this.subscribe(PLAYER_2_INPUTS.MOVING_UP);
        this.subscribe(PLAYER_2_INPUTS.MOVING_LEFT);
        this.subscribe(PLAYER_2_INPUTS.MOVING_RIGHT);
        this.subscribe(PLAYER_2_INPUTS.MOVING_DOWN);
    }

    onMessage(msg: Msg) {
        let data = msg.data;
        let posX = this.adjustX(data.posX);
        let poxY = this.adjustY(data.posY);

        this.playersPositions[data.playerId] = { posX: posX, posY: poxY };
        //console.log(this.playersPositions);
    }

    onUpdate(delta: number, absolute: number) {
        // till first player input
        if (!this.playersPositions.length) return;

        for (let i = 0; i < GAME_WIDTH; i++) {
            for (let j = 0; j < GAME_HEIGHT; j++) {
                if (!this.monster_map[i][j]) continue;

                let monster = this.monster_map[i][j];
                let monsterData = monster.getData();

                // Select which player to chase
                if (!monsterData.playerId) {
                    let playerId = this.selectPlayer();

                    // Player did not move yet, we set his starting position
                    if (!this.playersPositions[playerId]) {
                        let gameLevel = GAME_MAPS['game_level_' + this.GAME_LEVEL];
                        this.playersPositions[playerId] = gameLevel.players[playerId];
                    }

                    let next = this.whereToGoNext({ "posX": monster.getPosX(), "posY": monster.getPosY() }, this.playersPositions[playerId] );
                    
                    monster.setData("playerId", playerId);
                    monster.setData("next", next);
                    monster.setData("updated", absolute);
                    continue;
                }

                if (absolute - monsterData.updated > this.UPDATE_SPEED) {
                    let playerId = monsterData.playerId;
                    let next = this.whereToGoNext({ "posX": monster.getPosX(), "posY": monster.getPosY() }, this.playersPositions[playerId] );
                    
                    monster.setData("next", next);
                    monster.setData("updated", absolute);
                } else {
                    let next = monsterData.next;

                    if (monster.getPosY() < next.posY) {
                        monster.getPixiSprite().position.y += delta * this.MOVE_SPEED;
                        if ((monster.getPixiSprite().position.y - (next.posY*TEXTURE_SIZE + TEXTURE_OFFSET_TOP)) > 0) {
                            monster.setPosY(next.posY);
                        }
                    }
                    else if (monster.getPosY() > next.posY) {
                        monster.getPixiSprite().position.y -= delta * this.MOVE_SPEED;
                        if ((monster.getPixiSprite().position.y - (next.posY*TEXTURE_SIZE + TEXTURE_OFFSET_TOP)) < 0) {
                            monster.setPosY(next.posY);
                        }
                    }
                    else if (monster.getPosX() < next.posX) {
                        monster.getPixiSprite().position.x += delta * this.MOVE_SPEED;
                        if ((monster.getPixiSprite().position.x - (next.posX*TEXTURE_SIZE + TEXTURE_OFFSET_LEFT)) > 0) {
                            monster.setPosX(next.posX);
                        }
                    }
                    else if (monster.getPosX() > next.posX) {
                        monster.getPixiSprite().position.x -= delta * this.MOVE_SPEED;
                        if ((monster.getPixiSprite().position.x - (next.posX*TEXTURE_SIZE + TEXTURE_OFFSET_LEFT)) < 0) {
                            monster.setPosX(next.posX);
                        }
                    } 
                }

                // Check monster collisions
                for (let i = 1; i <= this.PLAYERS; i++) {
                    if (this.playersPositions[i].posX == monster.getPosX() && this.playersPositions[i].posY == monster.getPosY()) {
                        this.sendMessage(SOUNDS.DIE, { "playerId": i }); // -> soundComponent
                        this.sendMessage(PLAYER_COLLISIONS.DIE, { "playerId": i }); // -> dieComponent
                        this.sendMessage(PLAYER_STATS.LIFES_MINUS, { "playerId": i }); // -> scoreComponent
                    }
                }
            }
        }        
    }

    private whereToGoNext(src: any, dest: any) {
        let node = this.BFS(src, dest);

        // No path found, just go to a random spot or not move at all
        if (node == null) {
            let surr = [];
            let neighbours = [{"posX": -1, "posY": 0},{"posX": 0, "posY": -1},{"posX": 0, "posY": 1},{"posX": 1, "posY": 0}]; 

            surr.push(src);
            for (let i = 0; i < 4; i++) { 
                let posX = src.posX + neighbours[i].posX; 
                let posY = src.posY + neighbours[i].posY; 
                
                if (this.isInMap(posX, posY) && this.game_map[posX][posY].getGameType() == GAME_TYPES.BACKGROUND_TYPE && !this.bomb_map[posX][posY]) { 
                    surr.push({"posX": posX, "posY": posY});
                } 
            }

            // Return random nearby spot
            return surr[Math.floor(Math.random() * surr.length)];
        } else {
            let distance = node.distance;
            for (let i = 0; i < distance - 1; i++) {
                node = node.parent;
            }

            return node.position;
        }
    }

    private BFS(src: any, dest: any) {
        let neighbours = [{"posX": -1, "posY": 0},{"posX": 0, "posY": -1},{"posX": 0, "posY": 1},{"posX": 1, "posY": 0}]; 
         
        // Visited nodes array
        let visited = [];
        for (let i = 0; i < GAME_WIDTH; i++) {
            visited[i] = [];
            for (let j = 0; j < GAME_HEIGHT; j++) {
                visited[i][j] = false;
            }
        }
        
        // Mark the source cell as visited 
        visited[src.posX][src.posY] = true; 

        // Create a queue for BFS 
        let queue: Queue<Node> = new Queue; 
        
        // Distance of source cell is 0 
        let startNode = new Node(src, 0); 
        queue.add(startNode);  // Enqueue source cell 

        // Do a BFS starting from source cell 
        while (queue.size() != 0) { 
            let curr = queue.peek(); 
            let point = curr.position; 

            // If we have reached the destination cell, 
            if (point.posX == dest.posX && point.posY == dest.posY) {
                return curr;
            }

            // Otherwise dequeue the front cell in the queue 
            queue.dequeue(); 

            for (let i = 0; i < 4; i++) { 
                let row = point.posX + neighbours[i].posX; 
                let col = point.posY + neighbours[i].posY; 
                
                // Clear path
                if (this.isInMap(row, col) && this.game_map[row][col].getGameType() == GAME_TYPES.BACKGROUND_TYPE && !this.bomb_map[row][col] && !visited[row][col]) { 
                    // mark cell as visited and enqueue it 
                    visited[row][col] = true; 
                    let N = new Node({"posX": row, "posY": col}, curr.distance + 1, curr); 
                    queue.add(N); 
                    //console.log("added: x" + row + ", y:" + col);
                } 
            } 
        } 

        // Return null if theres walls in path 
        return null; 
    }

    private selectPlayer() {
        return Math.floor(Math.random() * this.PLAYERS) + 1;
    }

    // Adjusts to [x, y] position
    private adjustX(posX: number): number {
        let x = (Math.round((posX - TEXTURE_OFFSET_LEFT) / TEXTURE_SIZE) * TEXTURE_SIZE) + TEXTURE_OFFSET_LEFT;
        return (x - TEXTURE_OFFSET_LEFT) / TEXTURE_SIZE;
    }

    private adjustY(posY: number): number {
        let y = (Math.round((posY) / TEXTURE_SIZE) * TEXTURE_SIZE);
        return (y - TEXTURE_OFFSET_TOP) / TEXTURE_SIZE;
    }

    private isInMap(posX: number, posY: number): boolean {
        if (posX >= 0 && posX < GAME_WIDTH)
            if (posY >= 0 && posY < GAME_HEIGHT)
                return true;
               
        return false;        
    }
}

