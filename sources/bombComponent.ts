import { EXPLOSION_TEXTURES, PLAYER_1_INPUTS, PLAYER_2_INPUTS, PLAYER_STATS, GAME_MAPS, ANIMATION_EXIT, OBJECT_FACTORY, ANIMATION_ACTIONS, ANIMATION_MONSTERS, MONSTER_ANIMATION_ACTIONS, ANIMATION_BOMB_POWERUP } from './constants';
import { TEXTURE_BOMB, TEXTURE_OFFSET_LEFT, TEXTURE_OFFSET_TOP, TEXTURE_SIZE, GAME_TYPES, GAME_WIDTH, GAME_HEIGHT, TEXTURE_BACKGROUND, SOUNDS } from './constants';
import Component from '../ts/engine/Component';
import { GameObject } from './gameObject';
import Scene from '../ts/engine/Scene';
import Msg from '../ts/engine/Msg';
import { PIXICmp } from '../ts/engine/PIXIObject';
import PIXIObjectBuilder from '../ts/engine/PIXIObjectBuilder';

export class BombComponent extends Component {
    BOMB_LIFE_SPAN: number = 3000; // ms
    EXPLOSION_TEXTURE_SPEED: number = 100; // ms
    BOMB_TEXTURE_SPEED: number = 333; // ms
    BOMB_MAX_COUNT: number = 1;
    BOMB_COUNT: number = 0;
    BOMB_EXPLOSION_LENGTH: number = 1;

    GAME_LEVEL: number = null; 

    private game_map: Array<Array<GameObject>> = null; 
    private bomb_map: Array<Array<GameObject>> = null; 
    private monster_map: Array<Array<GameObject>> = null; 
    private playerId: number;

    constructor(scene: Scene, game_map: Array<Array<GameObject>>, bomb_map: Array<Array<GameObject>>, monster_map: Array<Array<GameObject>>, playerId: number) {
        super();
        this.scene = scene;
        this.game_map = game_map;
        this.bomb_map = bomb_map;
        this.monster_map = monster_map;
        this.playerId = playerId;
        this.GAME_LEVEL = scene.getGlobalAttribute("level");

        this.subscribe(PLAYER_1_INPUTS.DROP_BOMB);
        this.subscribe(PLAYER_2_INPUTS.DROP_BOMB);
        this.subscribe(PLAYER_STATS.BOMB_MAX_COUNT_PLUS);
        this.subscribe(PLAYER_STATS.EXPLOSION_LENGTH_PLUS);
    }

    onMessage(msg: Msg) {
        if (msg.action === PLAYER_1_INPUTS.DROP_BOMB || msg.action === PLAYER_2_INPUTS.DROP_BOMB) {
            if (this.playerId === msg.data.playerId) {
                this.tryToDropBomb(msg.data.posX, msg.data.posY);
            }
        }
        if (msg.action === PLAYER_STATS.BOMB_MAX_COUNT_PLUS) {
            this.BOMB_MAX_COUNT ++;
        }
        if (msg.action === PLAYER_STATS.EXPLOSION_LENGTH_PLUS) {
            this.BOMB_EXPLOSION_LENGTH ++;
        }
    }

    onUpdate(delta: number, absolute: number) {
        if (!this.BOMB_COUNT) return;

        for (let i = 0; i < GAME_WIDTH; i++) {
            for (let j = 0; j < GAME_HEIGHT; j++) {
                if (!this.bomb_map[i][j]) continue;

                if (!this.bomb_map[i][j].getCreated()) {
                    this.bomb_map[i][j].setCreated(absolute);
                    continue;
                }

                if (this.bomb_map[i][j].getCreated() + this.BOMB_LIFE_SPAN < absolute) {
                    // Update Count
                    this.BOMB_COUNT --;

                    // Makes explosion
                    this.makeExplosion(i, j);
                } else {
                    // Change texture
                    let elapsed = absolute - this.bomb_map[i][j].getCreated();
                    let bombTexture = this.getBombTextureByTimeElapsed(elapsed);
                    
                    let texture = PIXI.Texture.fromImage(bombTexture);
                    let sprite = <PIXI.Sprite> this.bomb_map[i][j].getPixiSprite();
                    sprite.texture = texture;
                }
            }
        }
    }

    private tryToDropBomb(posX: number, posY: number) {
        let bombX = this.adjustX();
        let bombY = this.adjustY();

        if (this.game_map[bombX][bombY].getGameType() === GAME_TYPES.BACKGROUND_TYPE) {
            if (!this.bomb_map[bombX][bombY]) {
                if (this.BOMB_COUNT < this.BOMB_MAX_COUNT) {
                    // Update Count
                    this.BOMB_COUNT ++;
                    
                    let data = { type: GAME_TYPES.BOMB_TYPE, position: { posX: bombX, posY: bombY }, gameLevel: this.GAME_LEVEL, swap: true, textureTag: TEXTURE_BOMB + 1, ownerObj: this.owner.getPixiObj()} 
                    this.sendMessage(OBJECT_FACTORY.CREATE, data); // -> objectFactoryComponent
                }
            }
        }
    }

    private makeExplosion(bombX: number, bombY: number) { 
        let neighbours = [{"posX": -1, "posY": 0},{"posX": 0, "posY": -1},{"posX": 0, "posY": 1},{"posX": 1, "posY": 0}]; 
        let pixiObj = this.bomb_map[bombX][bombY].getPixiSprite();
        this.bomb_map[bombX][bombY] = null;

        this.animateExplosion(pixiObj, 'explosion_' + EXPLOSION_TEXTURES.CENTER_TEXTURE);
        this.sendMessage(SOUNDS.BOMB); // -> soundComponent
        
        // Clear path
        if (this.isInMap(bombX+1, bombY) && this.isClearPath(bombX+1, bombY)) {
            let ex = 'explosion_' + EXPLOSION_TEXTURES.RIGHT_TEXTURE;
            let texture = PIXI.Texture.fromImage(ex);

            let expdObj = new PIXICmp.Sprite(ex, texture);

            this.placeExplosion(bombX+1, bombY, expdObj);
            this.animateExplosion(expdObj, 'explosion_' + EXPLOSION_TEXTURES.RIGHT_TEXTURE);
            this.bomb_map[bombX+1][bombY] = null;
        }
        if (this.isInMap(bombX-1, bombY) && this.isClearPath(bombX-1, bombY)) {
            let ex = 'explosion_' + EXPLOSION_TEXTURES.LEFT_TEXTURE;
            let texture = PIXI.Texture.fromImage(ex);

            let expdObj = new PIXICmp.Sprite(ex, texture);
            
            this.placeExplosion(bombX-1, bombY, expdObj);
            this.animateExplosion(expdObj, 'explosion_' + EXPLOSION_TEXTURES.LEFT_TEXTURE);
            this.bomb_map[bombX-1][bombY] = null;
        }
        if (this.isInMap(bombX, bombY+1) && this.isClearPath(bombX, bombY+1)) {
            let ex = 'explosion_' + EXPLOSION_TEXTURES.BOTTOM_TEXTURE;
            let texture = PIXI.Texture.fromImage(ex);

            let expdObj = new PIXICmp.Sprite(ex, texture);
            
            this.placeExplosion(bombX, bombY+1, expdObj);
            this.animateExplosion(expdObj, 'explosion_' + EXPLOSION_TEXTURES.BOTTOM_TEXTURE);
            this.bomb_map[bombX][bombY+1] = null;
        }
        if (this.isInMap(bombX, bombY-1) && this.isClearPath(bombX, bombY-1)) {
            let ex = 'explosion_' + EXPLOSION_TEXTURES.TOP_TEXTURE;
            let texture = PIXI.Texture.fromImage(ex);

            let expdObj = new PIXICmp.Sprite(ex, texture);
            
            this.placeExplosion(bombX, bombY-1, expdObj);
            this.animateExplosion(expdObj, 'explosion_' + EXPLOSION_TEXTURES.TOP_TEXTURE);
            this.bomb_map[bombX][bombY-1] = null;
        }

        // Obstacle in its path
        for (let i = 0; i < 4; i++) { 
            let posX = bombX + neighbours[i].posX; 
            let posY = bombY + neighbours[i].posY; 
            
            if (this.isInMap(posX, posY) && (!this.isClearPath(posX, posY) || this.isThereAMonster(posX, posY))) { 
                this.animateAndReplaceObstacle(posX, posY);
            }
        }
    }

    private placeExplosion(bombX: number, bombY: number, expObj: PIXICmp.Sprite) {
        let posX = bombX*TEXTURE_SIZE + TEXTURE_OFFSET_LEFT;
        let posY = bombY*TEXTURE_SIZE + TEXTURE_OFFSET_TOP;

        let gameObj = new GameObject(bombX, bombY, true, expObj, GAME_TYPES.EXPLOSION_TYPE);
        this.bomb_map[bombX][bombY] = gameObj;

        new PIXIObjectBuilder(this.scene).globalPos(posX, posY).anchor(0.5, 0.5).build(expObj);
        this.scene.stage.getPixiObj().addChild(expObj); 
    }

    private animateExplosion(pixiObj: PIXICmp.Sprite, exp: string) {

        for (let i = 1; i <= 5; i++) {
            setTimeout(() => {
                if (i == 5) {
                    pixiObj.destroy(); // 5th tick destroys pixiObj
                    return;
                }

                let ex = exp + i;
                let texture = PIXI.Texture.fromImage(ex);
                pixiObj.texture = texture;
            }, i*this.EXPLOSION_TEXTURE_SPEED);
        }
    }

    private animateAndReplaceObstacle(posX: number, posY: number) {
        // If wall
        if (this.game_map[posX][posY].getGameType() === GAME_TYPES.WALL_TYPE) {
            let pixiObj = this.game_map[posX][posY].getPixiSprite();
            let background = TEXTURE_BACKGROUND + this.GAME_LEVEL;
            let score = this.game_map[posX][posY].getScore(); 

            this.sendMessage(PLAYER_STATS.SCORE_PLUS, { score: score }); // -> scoreComponent

            for (let i = 1; i <= 5; i++) {
                setTimeout(() => {

                    // last frame
                    if (i == 5) {
                        // EXIT
                        if (GAME_MAPS["game_level_" + this.GAME_LEVEL].exit.x == posX && GAME_MAPS["game_level_" + this.GAME_LEVEL].exit.y == posY) {
                            let data = { type: GAME_TYPES.EXIT_TYPE, position: { posX: posX, posY: posY }, textureTag: ANIMATION_EXIT[0] }; 
                            this.sendMessage(OBJECT_FACTORY.REPLACE, data); // -> objectFactoryComponent

                            let data2 = { spriteId: this.game_map[posX][posY].getPixiSprite().getId(), position: { posX: posX, posY: posY }, textures: ANIMATION_EXIT };
                            this.sendMessage(ANIMATION_ACTIONS.ANIMATE, data2); // -> textureAnimationComponent
                            return;
                        }

                        // POWERUP
                        if (GAME_MAPS["game_level_" + this.GAME_LEVEL].powerUp.x == posX && GAME_MAPS["game_level_" + this.GAME_LEVEL].powerUp.y == posY) {
                            let data = { type: GAME_TYPES.POWER_UP_BOMB_TYPE, position: { posX: posX, posY: posY }, textureTag: ANIMATION_BOMB_POWERUP[0] }; 
                            this.sendMessage(OBJECT_FACTORY.REPLACE, data); // -> objectFactoryComponent

                            let data2 = { spriteId: this.game_map[posX][posY].getPixiSprite().getId(), position: { posX: posX, posY: posY }, textures: ANIMATION_BOMB_POWERUP };
                            this.sendMessage(ANIMATION_ACTIONS.ANIMATE, data2); // -> textureAnimationComponent
                            return;
                        }

                        // Destroy wall
                        let data = { type: GAME_TYPES.BACKGROUND_TYPE, position: { posX: posX, posY: posY }, textureTag: background }; 
                        this.sendMessage(OBJECT_FACTORY.REPLACE, data); // -> objectFactoryComponent
                        return;
                    }
    
                    let w = background + i;
                    let texture = PIXI.Texture.fromImage(w);
                    pixiObj.texture = texture;
                }, i*this.EXPLOSION_TEXTURE_SPEED);
            }
        }

        // if poverUp
        if (this.game_map[posX][posY].getGameType() === GAME_TYPES.POWER_UP_BOMB_TYPE) {
            let data = { type: GAME_TYPES.BACKGROUND_TYPE, position: { posX: posX, posY: posY }, textureTag: TEXTURE_BACKGROUND + this.GAME_LEVEL }; 
            this.sendMessage(ANIMATION_ACTIONS.STOP_ANIMATE, { "spriteId": this.game_map[posX][posY].getPixiSprite().getId() }); // -> textureAnimationComponent
            this.sendMessage(OBJECT_FACTORY.REPLACE, data); // -> objectFactoryComponent
        }

        // If monster
        if (this.isThereAMonster(posX, posY)) {
            let monsterPositions = this.getMonsterPosition(posX, posY);

            for (let position of monsterPositions) {
                let monster = this.monster_map[position.posX][position.posY];
                let pixiObj = monster.getPixiSprite();
                let score = monster.getScore(); 
                let textures = ANIMATION_MONSTERS[monster.getData("monsterType")].dead;

                this.sendMessage(PLAYER_STATS.SCORE_PLUS, { score: score }); // -> scoreComponent 

                // 1) stop monster
                this.monster_map[position.posX][position.posY] = null;

                // 2) cancel animation
                this.sendMessage(MONSTER_ANIMATION_ACTIONS.STOP_ANIMATE, { "spriteId": pixiObj.getId() });

                // 3) set dead texture
                let texture = PIXI.Texture.fromImage(textures[0]);
                pixiObj.texture = texture;
                
                for (let i = 5; i <= 8; i++) {
                    setTimeout(() => {
                        // last frame
                        if (i == 8) {
                            pixiObj.destroy();
                            return;
                        }
                        
                        let w = textures[i-5];
                        let texture = PIXI.Texture.fromImage(w);
                        pixiObj.texture = texture;
                    }, i*this.EXPLOSION_TEXTURE_SPEED);
                }
            }
        }
    }

    private isInMap(posX: number, posY: number): boolean {
        if (posX >= 0 && posX < GAME_WIDTH)
            if (posY >= 0 && posY < GAME_HEIGHT)
                return true;
               
        return false;        
    }

    private isClearPath(posX: number, posY: number): boolean {
        if (this.game_map[posX][posY].getGameType() === GAME_TYPES.BACKGROUND_TYPE)
            if (!this.bomb_map[posX][posY])
                return true;

        return false;
    }

    private isThereAMonster(posX: number, posY: number): boolean {
        for (let i = 0; i < GAME_WIDTH; i++) {
            for (let j = 0; j < GAME_HEIGHT; j++) { 
                if (!this.monster_map[i][j]) continue;

                let monster = this.monster_map[i][j];
                if (monster.getPosX() == posX && monster.getPosY() == posY) {
                    return true;
                }
            }
        }

        return false;
    }

    private getMonsterPosition(posX: number, posY: number): any {
        let positions = []; // There can be more monsters on the same spot

        for (let i = 0; i < GAME_WIDTH; i++) {
            for (let j = 0; j < GAME_HEIGHT; j++) { 
                if (!this.monster_map[i][j]) continue;

                let monster = this.monster_map[i][j];
                if (monster.getPosX() == posX && monster.getPosY() == posY) {
                    positions.push({ "posX": i, "posY": j });
                }
            }
        }

        return positions;
    }

    private adjustX(): number {
        let x = (Math.round((this.owner.getPixiObj().position.x - TEXTURE_OFFSET_LEFT) / TEXTURE_SIZE) * TEXTURE_SIZE) + TEXTURE_OFFSET_LEFT;
        return (x - TEXTURE_OFFSET_LEFT) / TEXTURE_SIZE;
    }

    private adjustY(): number {
        let y = (Math.round((this.owner.getPixiObj().position.y) / TEXTURE_SIZE) * TEXTURE_SIZE);
        return (y - TEXTURE_OFFSET_TOP) / TEXTURE_SIZE;
    }

    private getBombTextureByTimeElapsed(elapsed: number): string {
        let i = 1;
        if (elapsed % 1000 < this.BOMB_TEXTURE_SPEED) i = 1;
        else if (elapsed % 1000 < 2*this.BOMB_TEXTURE_SPEED) i = 2;
        else i = 3;

        return TEXTURE_BOMB + i;
    }
}