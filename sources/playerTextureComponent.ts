import { PLAYER_1_INPUTS, PLAYER_2_INPUTS, PLAYER_TEXTURES } from './constants';
import Component from '../ts/engine/Component';
import Scene from '../ts/engine/Scene';
import Msg from '../ts/engine/Msg';
import { PIXICmp } from '../ts/engine/PIXIObject';

export class PlayerTextureComponent extends Component {
    TEXTURE_SPEED: number = 300; // ms
    private last_move: string = 'none';
    private actual_move: string = 'none';
    private playerId: number;

    constructor(scene: Scene, playerId: number) {
        super();
        this.scene = scene;
        this.playerId = playerId;
        this.actual_move = 'P' +  this.playerId + '_none';
        this.last_move = 'P' +  this.playerId + '_none';

        this.subscribe(PLAYER_1_INPUTS.EMPTY);
        this.subscribe(PLAYER_1_INPUTS.MOVING_UP);
        this.subscribe(PLAYER_1_INPUTS.MOVING_RIGHT);
        this.subscribe(PLAYER_1_INPUTS.MOVING_DOWN);
        this.subscribe(PLAYER_1_INPUTS.MOVING_LEFT);

        this.subscribe(PLAYER_2_INPUTS.EMPTY);
        this.subscribe(PLAYER_2_INPUTS.MOVING_UP);
        this.subscribe(PLAYER_2_INPUTS.MOVING_RIGHT);
        this.subscribe(PLAYER_2_INPUTS.MOVING_DOWN);
        this.subscribe(PLAYER_2_INPUTS.MOVING_LEFT);
    }

    onMessage(msg: Msg) {
        if (msg.data.playerId == this.playerId) {
            this.last_move = this.actual_move;
            this.actual_move = msg.action;
        }
    }

    onUpdate(delta: number, absolute: number) {
        if (this.playerId == 1)
            this.tryUpdateP1(delta, absolute);
        if (this.playerId == 2)    
            this.tryUpdateP2(delta, absolute);
    }

    private tryUpdateP1(delta: number, absolute: number) {
        if (this.last_move === this.actual_move && this.last_move === PLAYER_1_INPUTS.EMPTY) return;

        absolute = Math.round(absolute / this.TEXTURE_SPEED);
        let i = Math.round(absolute % 2) + 2;

        let playerTexture = '';
        
        // Keys freed
        if (this.actual_move === PLAYER_1_INPUTS.EMPTY) {
            if (this.last_move === PLAYER_1_INPUTS.MOVING_UP)
                playerTexture = PLAYER_TEXTURES.BACK_TEXTURE + 1;
            if (this.last_move === PLAYER_1_INPUTS.MOVING_RIGHT)
                playerTexture = PLAYER_TEXTURES.RIGHT_TEXTURE + 1;
            if (this.last_move === PLAYER_1_INPUTS.MOVING_DOWN)
                playerTexture = PLAYER_TEXTURES.FORW_TEXTURE + 1;
            if (this.last_move === PLAYER_1_INPUTS.MOVING_LEFT)
                playerTexture = PLAYER_TEXTURES.LEFT_TEXTURE + 1;            
        }

        // Keys pressed
        if (this.actual_move === PLAYER_1_INPUTS.MOVING_UP) {
            playerTexture = PLAYER_TEXTURES.BACK_TEXTURE + i;
        }
        if (this.actual_move === PLAYER_1_INPUTS.MOVING_RIGHT) {
            playerTexture = PLAYER_TEXTURES.RIGHT_TEXTURE + i;
        }
        if (this.actual_move === PLAYER_1_INPUTS.MOVING_DOWN) {
            playerTexture = PLAYER_TEXTURES.FORW_TEXTURE + i;
        }
        if (this.actual_move === PLAYER_1_INPUTS.MOVING_LEFT) {
            playerTexture = PLAYER_TEXTURES.LEFT_TEXTURE + i;
        }

        let texture = PIXI.Texture.fromImage(playerTexture);
        let sprite = <PIXI.Sprite> this.owner.getPixiObj();
        sprite.texture = texture;
    }
    
    private tryUpdateP2(delta: number, absolute: number) {
        if (this.last_move === this.actual_move && this.last_move === PLAYER_2_INPUTS.EMPTY) return;

        absolute = Math.round(absolute / this.TEXTURE_SPEED);
        let i = Math.round(absolute % 2) + 2;

        let playerTexture = '';

        // Keys freed
        if (this.actual_move === PLAYER_2_INPUTS.EMPTY) {
            if (this.last_move === PLAYER_2_INPUTS.MOVING_UP)
                playerTexture = PLAYER_TEXTURES.BACK_TEXTURE + 1;
            if (this.last_move === PLAYER_2_INPUTS.MOVING_RIGHT)
                playerTexture = PLAYER_TEXTURES.RIGHT_TEXTURE + 1;
            if (this.last_move === PLAYER_2_INPUTS.MOVING_DOWN)
                playerTexture = PLAYER_TEXTURES.FORW_TEXTURE + 1;
            if (this.last_move === PLAYER_2_INPUTS.MOVING_LEFT)
                playerTexture = PLAYER_TEXTURES.LEFT_TEXTURE + 1;            
        }

        // Keys pressed
        if (this.actual_move === PLAYER_2_INPUTS.MOVING_UP) {
            playerTexture = PLAYER_TEXTURES.BACK_TEXTURE + i;
        }
        if (this.actual_move === PLAYER_2_INPUTS.MOVING_RIGHT) {
            playerTexture = PLAYER_TEXTURES.RIGHT_TEXTURE + i;
        }
        if (this.actual_move === PLAYER_2_INPUTS.MOVING_DOWN) {
            playerTexture = PLAYER_TEXTURES.FORW_TEXTURE + i;
        }
        if (this.actual_move === PLAYER_2_INPUTS.MOVING_LEFT) {
            playerTexture = PLAYER_TEXTURES.LEFT_TEXTURE + i;
        }

        let texture = PIXI.Texture.fromImage(playerTexture);
        let sprite = <PIXI.Sprite> this.owner.getPixiObj();
        sprite.texture = texture;
    }
}