import { PLAYER_1_INPUTS, PLAYER_2_INPUTS, SOUNDS } from './constants';
import Component from '../ts/engine/Component';
import Scene from '../ts/engine/Scene';
import Msg from '../ts/engine/Msg';
import PIXIObjectBuilder from '../ts/engine/PIXIObjectBuilder';
import { PIXICmp } from '../ts/engine/PIXIObject';

export class SoundComponent extends Component {
    IS_PLAYING: boolean = false;

    constructor(scene: Scene) {
        super();
        this.scene = scene;

        this.subscribe(PLAYER_1_INPUTS.PAUSE);
        this.subscribe(PLAYER_1_INPUTS.MOVING_UP);
        this.subscribe(PLAYER_1_INPUTS.MOVING_LEFT);
        this.subscribe(PLAYER_1_INPUTS.MOVING_RIGHT);
        this.subscribe(PLAYER_1_INPUTS.MOVING_DOWN);
        this.subscribe(PLAYER_2_INPUTS.MOVING_UP);
        this.subscribe(PLAYER_2_INPUTS.MOVING_LEFT);
        this.subscribe(PLAYER_2_INPUTS.MOVING_RIGHT);
        this.subscribe(PLAYER_2_INPUTS.MOVING_DOWN);

        this.subscribe(SOUNDS.BOMB);
        this.subscribe(SOUNDS.DIE);
        this.subscribe(SOUNDS.GAME);
        this.subscribe(SOUNDS.POWERUP);
        this.subscribe(SOUNDS.WIN);
    }

    onMessage(msg: Msg) {
        if (msg.action !== PLAYER_1_INPUTS.PAUSE) {
            if (!this.IS_PLAYING) {
                this.IS_PLAYING = true;
                PIXI.sound.play(SOUNDS.GAME);
                PIXI.sound.stop(SOUNDS.MENU);
            }
        }

        if (msg.action === SOUNDS.BOMB) {
            PIXI.sound.play(SOUNDS.BOMB);
        }
        if (msg.action === SOUNDS.POWERUP) {
            PIXI.sound.play(SOUNDS.POWERUP);
        }
        if (msg.action === SOUNDS.DIE) {
            PIXI.sound.stop(SOUNDS.DIE);
            PIXI.sound.play(SOUNDS.DIE);
        }
        if (msg.action === SOUNDS.WIN) {
            PIXI.sound.stop(SOUNDS.GAME);
            PIXI.sound.play(SOUNDS.WIN);
        }
        if (msg.action === SOUNDS.LOOSE) {
            PIXI.sound.stop(SOUNDS.GAME);
            PIXI.sound.play(SOUNDS.DIE);
        }
    }
    
    onInit() {
        
    }

    onUpdate(delta: number, absolute: number) {
        
    }
}