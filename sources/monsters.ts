import Scene from '../ts/engine/Scene';
import { ANIMATION_MONSTERS, GAME_MAPS, GAME_TYPES } from './constants';
import { TEXTURE_OFFSET_LEFT, TEXTURE_OFFSET_TOP, TEXTURE_SIZE } from './constants';
import { PIXICmp } from '../ts/engine/PIXIObject';
import PIXIObjectBuilder from '../ts/engine/PIXIObjectBuilder';
import { GameObject } from './gameObject';
import { MonsterTextureComponent } from './monsterTextureComponent'
import { MonsterMoveComponent } from './monsterMoveComponent'

export class Monsters {
    GAME_LEVEL: number = null;
    PLAYERS: number = null;

    private game_map: Array<Array<GameObject>> = null;
    private bomb_map: Array<Array<GameObject>> = null;
    private monster_map: Array<Array<GameObject>> = null;

    constructor(game_map: Array<Array<GameObject>>, bomb_map: Array<Array<GameObject>>, monster_map: Array<Array<GameObject>>) {
        this.game_map = game_map;
        this.bomb_map = bomb_map;
        this.monster_map = monster_map;
    }

    init(scene: Scene) {
        this.PLAYERS = scene.getGlobalAttribute("players");
        this.GAME_LEVEL = scene.getGlobalAttribute("level");

        let monsterContainer = new PIXICmp.Container();
        scene.stage.getPixiObj().addChild(monsterContainer); 

        // draw monsters
        let gameLevel = GAME_MAPS['game_level_' + this.GAME_LEVEL];
        for (let monster of gameLevel.monsters) {

            let textureTag = ANIMATION_MONSTERS[monster.type].live[0];
            let texture = PIXI.Texture.fromImage(textureTag);

            let monsterObj = new PIXICmp.Sprite(textureTag, texture);
            let posX = monster.posX*TEXTURE_SIZE + TEXTURE_OFFSET_LEFT;
            let posY = monster.posY*TEXTURE_SIZE + TEXTURE_OFFSET_TOP;

            new PIXIObjectBuilder(scene).globalPos(posX, posY).anchor(0.5, 0.5).build(monsterObj);
            monsterContainer.addChild(monsterObj); 

            this.monster_map[monster.posX][monster.posY] = new GameObject(monster.posX, monster.posY, true, monsterObj, GAME_TYPES.MONSTER_TYPE, this.GAME_LEVEL);
            this.monster_map[monster.posX][monster.posY].setScore(500);
            this.monster_map[monster.posX][monster.posY].setData("monsterType", monster.type);
        }

        monsterContainer.addComponent(new MonsterTextureComponent(scene, this.monster_map));
        monsterContainer.addComponent(new MonsterMoveComponent(scene, this.game_map, this.bomb_map, this.monster_map));
    }    
}