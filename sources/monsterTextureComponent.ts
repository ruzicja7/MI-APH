import { GAME_WIDTH, GAME_HEIGHT, ANIMATION_MONSTERS, MONSTER_ANIMATION_ACTIONS } from './constants';
import Component from '../ts/engine/Component';
import Scene from '../ts/engine/Scene';
import Msg from '../ts/engine/Msg';
import { GameObject } from './gameObject';
import { PIXICmp } from '../ts/engine/PIXIObject';

export class MonsterTextureComponent extends Component {
    DEFAULT_ANIMATION_SPEED: number = 300; //ms
    GAME_LEVEL: number = null;      

    private monster_map: Array<Array<GameObject>>;
    private objectsToAnimate = [];

    constructor(scene: Scene, monster_map: Array<Array<GameObject>>) {
        super();
        this.scene = scene; 
        this.monster_map = monster_map;
        this.GAME_LEVEL = scene.getGlobalAttribute("level");

        this.subscribe(MONSTER_ANIMATION_ACTIONS.STOP_ANIMATE);
    }

    onMessage(msg: Msg) {
        if (msg.action == MONSTER_ANIMATION_ACTIONS.STOP_ANIMATE) {
            this.stopAnimation(msg.data);
        }
    }

    onInit() {
        for (let i = 0; i < GAME_WIDTH; i++) {
            for (let j = 0; j < GAME_HEIGHT; j++) {
                if (this.monster_map[i][j]) {

                    let spriteId = this.monster_map[i][j].getPixiSprite().getId();
                    let monster = this.monster_map[i][j];
                    let textures = ANIMATION_MONSTERS[monster.getData("monsterType")].live;

                    let obj = { spriteId: spriteId, monster: monster, updated: 0, textures: textures, textureIndex: 0 };
                    this.objectsToAnimate[spriteId] = obj;
                } 
            }
        }
    }

    onUpdate(delta: number, absolute: number) {

        this.objectsToAnimate.forEach(obj => {
            if (obj) {
                if (obj.updated == 0) {
                    obj.updated = absolute;
                } else {
                    if (absolute - obj.updated > this.DEFAULT_ANIMATION_SPEED) {
                        obj.updated = absolute;
                        
                        obj.textureIndex = this.getNextIndex(obj.textureIndex, obj.textures.length);
                        let texture = PIXI.Texture.fromImage(obj.textures[obj.textureIndex]);
                        obj.monster.getPixiSprite().texture = texture;
                    }
                }
            }
        });
    }
    
    private stopAnimation(data: any) {
        delete this.objectsToAnimate[data.spriteId];
    }

    private getNextIndex(index: number, arrayLength: number): number {
        return (index < arrayLength-1) ? index+1 : 0; 
    }
}