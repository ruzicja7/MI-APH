import { GAME_WIDTH, GAME_HEIGHT, GAME_TYPES, GAME_MAPS } from './constants';
import { TEXTURE_BACKGROUND, TEXTURE_BRICK, TEXTURE_WALL, TEXTURE_SIZE, TEXTURE_OFFSET_LEFT, TEXTURE_OFFSET_TOP } from './constants';
import { TEXTURE_TABLE, TEXTURE_TABLE_DOWN, TEXTURE_TABLE_UP, TEXTURE_TABLE_LEFT, TEXTURE_TABLE_RIGHT, TEXTURE_TABLE_CORNER_LEFT, TEXTURE_TABLE_CORNER_RIGHT } from './constants';
import Scene from '../ts/engine/Scene';
import { PIXICmp } from '../ts/engine/PIXIObject';
import PIXIObjectBuilder from '../ts/engine/PIXIObjectBuilder';
import { GameObject } from './gameObject';
import { ObjectFactoryComponent } from './objectFactoryComponent'
import { TimeComponent } from './timeComponent'
import { ScoreComponent } from './scoreComponent'
import { TextureAnimationComponent } from './textureAnimationComponent'
import { SoundComponent } from './soundComponent'

export class Game {
    GAME_LEVEL: number = null;   
    PLAYERS: number = null;
    
    private game_map: Array<Array<GameObject>> = null;
    private bomb_map: Array<Array<GameObject>> = null;

    constructor(game_map: Array<Array<GameObject>>, bomb_map: Array<Array<GameObject>>) {
        this.game_map = game_map;
        this.bomb_map = bomb_map;
    }

    init(scene: Scene) {
        this.PLAYERS = scene.getGlobalAttribute("players");
        this.GAME_LEVEL = scene.getGlobalAttribute("level");

        // Game Borders
        this.drawBorders(scene);

        // Game Map
        // 13 x 13 = 169

        // 6 x 5 = 30
        this.drawBricks(scene);

        // 169 - 30 = 139
        this.drawBackgrounds(scene);

        // map from file
        this.drawWalls(scene);

        let gameContainer = new PIXICmp.Container();
        scene.stage.getPixiObj().addChild(gameContainer); 
        gameContainer.addComponent(new ObjectFactoryComponent(scene, this.game_map, this.bomb_map));
        gameContainer.addComponent(new TimeComponent(scene)); 
        gameContainer.addComponent(new ScoreComponent(scene));
        gameContainer.addComponent(new TextureAnimationComponent(scene, this.game_map));
        gameContainer.addComponent(new SoundComponent(scene));
    }

    private drawBorders(scene: Scene) {
        // Top
        let tableObj = new PIXICmp.Sprite(TEXTURE_TABLE, PIXI.Texture.fromImage(TEXTURE_TABLE));
        new PIXIObjectBuilder(scene).globalPos(0, 0).build(tableObj);
        scene.stage.getPixiObj().addChild(tableObj);

        let tableTopObj = new PIXICmp.Sprite(TEXTURE_TABLE_UP + this.GAME_LEVEL, PIXI.Texture.fromImage(TEXTURE_TABLE_UP + this.GAME_LEVEL));
        tableTopObj.scale.x = 1.15;
        new PIXIObjectBuilder(scene).globalPos(50, TEXTURE_OFFSET_TOP/2).build(tableTopObj);
        scene.stage.getPixiObj().addChild(tableTopObj);

        // Corners
        for (let i = 0; i < 2; i++) {
            let tableCornerObj = null;
            let posX = 0;
            let posY = TEXTURE_OFFSET_TOP/2;

            switch (i) {
                case 0: 
                    tableCornerObj = new PIXICmp.Sprite(TEXTURE_TABLE_CORNER_LEFT + this.GAME_LEVEL, PIXI.Texture.fromImage(TEXTURE_TABLE_CORNER_LEFT + this.GAME_LEVEL));
                    break;
                case 1:
                    tableCornerObj = new PIXICmp.Sprite(TEXTURE_TABLE_CORNER_RIGHT + this.GAME_LEVEL, PIXI.Texture.fromImage(TEXTURE_TABLE_CORNER_RIGHT + this.GAME_LEVEL));
                    posX = (GAME_WIDTH + 1) * TEXTURE_SIZE;
                    break;
            }

            new PIXIObjectBuilder(scene).globalPos(posX, posY).build(tableCornerObj);
            scene.stage.getPixiObj().addChild(tableCornerObj);
        }

        // Left
        let tableLeftObj = new PIXICmp.Sprite(TEXTURE_TABLE_LEFT + this.GAME_LEVEL, PIXI.Texture.fromImage(TEXTURE_TABLE_LEFT + this.GAME_LEVEL));
        tableLeftObj.scale.y = 1.6;
        new PIXIObjectBuilder(scene).globalPos(0, 200).build(tableLeftObj);
        scene.stage.getPixiObj().addChild(tableLeftObj); 

        tableLeftObj = new PIXICmp.Sprite(TEXTURE_TABLE_LEFT + this.GAME_LEVEL, PIXI.Texture.fromImage(TEXTURE_TABLE_LEFT + this.GAME_LEVEL));
        tableLeftObj.scale.y = 1.6;
        new PIXIObjectBuilder(scene).globalPos(0, 646).build(tableLeftObj);
        scene.stage.getPixiObj().addChild(tableLeftObj);

        // Right
        let tableRightObj = new PIXICmp.Sprite(TEXTURE_TABLE_RIGHT + this.GAME_LEVEL, PIXI.Texture.fromImage(TEXTURE_TABLE_RIGHT + this.GAME_LEVEL));
        tableRightObj.scale.y = 1.6;
        new PIXIObjectBuilder(scene).globalPos((GAME_WIDTH + 1) * TEXTURE_SIZE, 200).build(tableRightObj);
        scene.stage.getPixiObj().addChild(tableRightObj); 

        tableRightObj = new PIXICmp.Sprite(TEXTURE_TABLE_RIGHT + this.GAME_LEVEL, PIXI.Texture.fromImage(TEXTURE_TABLE_RIGHT + this.GAME_LEVEL));
        tableRightObj.scale.y = 1.6;
        new PIXIObjectBuilder(scene).globalPos((GAME_WIDTH + 1) * TEXTURE_SIZE, 646).build(tableRightObj);
        scene.stage.getPixiObj().addChild(tableRightObj);

        // Bottom
        let tableDownObj = new PIXICmp.Sprite(TEXTURE_TABLE_DOWN + this.GAME_LEVEL, PIXI.Texture.fromImage(TEXTURE_TABLE_DOWN + this.GAME_LEVEL));
        new PIXIObjectBuilder(scene).globalPos(0, 775).build(tableDownObj);
        scene.stage.getPixiObj().addChild(tableDownObj);
    }

    private drawBricks(scene: Scene) { 
        let brick = TEXTURE_BRICK + this.GAME_LEVEL;
        let texture = PIXI.Texture.fromImage(brick);

        for (let i = 0; i < GAME_WIDTH; i++) {
            for (let j = 0; j < GAME_HEIGHT; j++) {
                if (i % 2 == 1 && j % 2 == 1) {

                    let brickObj = new PIXICmp.Sprite(brick, texture);
                    let posX = i*TEXTURE_SIZE + TEXTURE_OFFSET_LEFT;
                    let posY = j*TEXTURE_SIZE + TEXTURE_OFFSET_TOP;

                    new PIXIObjectBuilder(scene).globalPos(posX, posY).anchor(0.5, 0.5).build(brickObj);
                    scene.stage.getPixiObj().addChild(brickObj); 
                    
                    this.game_map[i][j] = new GameObject(i, j, true, brickObj, GAME_TYPES.BRICK_TYPE, this.GAME_LEVEL);
                }    
            }
        } 
    }

    private drawBackgrounds(scene: Scene) {
        let background = TEXTURE_BACKGROUND + this.GAME_LEVEL;
        let texture = PIXI.Texture.fromImage(background);

        for (let i = 0; i < GAME_WIDTH; i++) {
            for (let j = 0; j < GAME_HEIGHT; j++) {
                if (i % 2 == 1 && j % 2 == 1) continue;

                let backgroundObj = new PIXICmp.Sprite(background, texture);
                let posX = i*TEXTURE_SIZE + TEXTURE_OFFSET_LEFT;
                let posY = j*TEXTURE_SIZE + TEXTURE_OFFSET_TOP;

                new PIXIObjectBuilder(scene).globalPos(posX, posY).anchor(0.5, 0.5).build(backgroundObj);
                scene.stage.getPixiObj().addChild(backgroundObj); 

                this.game_map[i][j] = new GameObject(i, j, true, backgroundObj, GAME_TYPES.BACKGROUND_TYPE, this.GAME_LEVEL);  
            }
        }    
    }

    private drawWalls(scene: Scene) {
        let wall = TEXTURE_WALL + this.GAME_LEVEL;
        let texture = PIXI.Texture.fromImage(wall);

        let gameLevel = GAME_MAPS['game_level_' + this.GAME_LEVEL];
        for (let wall of gameLevel.walls) {

            // Destroy background
            this.game_map[wall.x][wall.y].getPixiSprite().texture = texture;
            this.game_map[wall.x][wall.y].setGameType(GAME_TYPES.WALL_TYPE);
            this.game_map[wall.x][wall.y].setScore(100);
        }
    }
}