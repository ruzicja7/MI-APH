// --- GAME ---
export const GAME_LEVELS: number = 2;
export const GAME_HEIGHT: number = 13;
export const GAME_WIDTH: number = 13;

export const TEXTURE_SIZE: number = 50;
export const TEXTURE_OFFSET_TOP: number = 150;
export const TEXTURE_OFFSET_RIGHT: number = 50;
export const TEXTURE_OFFSET_BOTTOM: number = 75;
export const TEXTURE_OFFSET_LEFT: number = 75;

// --- PLAYER MOVES ---
export const enum PLAYER_1_INPUTS {
    EMPTY = "P1_none",
    MOVING_UP = "P1_moving_up",
    MOVING_DOWN = "P1_moving_down",
    MOVING_LEFT = "P1_moving_left",
    MOVING_RIGHT = "P1_moving_right",
    DROP_BOMB = "P1_drop_bomb",
    PAUSE = "pause"
}

export const enum PLAYER_2_INPUTS {
    EMPTY = "P2_none",
    MOVING_UP = "P2_moving_up",
    MOVING_DOWN = "P2_moving_down",
    MOVING_LEFT = "P2_moving_left",
    MOVING_RIGHT = "P2_moving_right",
    DROP_BOMB = "P2_drop_bomb",
    PAUSE = "pause"
}

// --- PLAYER STATS ---
export const enum PLAYER_STATS {
    BOMB_MAX_COUNT_PLUS = "bomb_count_plus",
    BOMB_MAX_COUNT_MINUS = "bomb_count_minus",
    LIFES_PLUS = "lifes_plus",
    LIFES_MINUS = "lifes_minus",
    EXPLOSION_LENGTH_PLUS = "explosin_length_plus",
    EXPLOSION_LENGTH_MINUS = "explosin_length_minus",
    SCORE_PLUS = "score_plus"
}

// --- PLAYER COLLISIONS ---
export const enum PLAYER_COLLISIONS {
    TOP = "collision_top",
    RIGHT = "collision_right",
    BOTTOM = "collision_bottom",
    LEFT = "collision_left",
    RESET = "collisin_reset",
    DIE = "collision_and_players_dead"
}

// --- GAME SOUNDS ---
export const enum SOUNDS {
    WIN = "sound_win",
    LOOSE = "sound_loose",
    MENU = "sound_menu",
    BOMB = "sound_bomb",
    GAME = "sound_game",
    POWERUP = "sound_powerup",
    DIE = "sound_die"
}

// --- OBJECT TYPES ---
export const enum GAME_TYPES {
    WALL_TYPE = 'wall', //Explodable -> BACKGROUND_TYPE
    BRICK_TYPE = 'brick', //Static
    BACKGROUND_TYPE = 'background', //Wallkable
    BOMB_TYPE = 'bomb',
    EXPLOSION_TYPE = 'explosion',
    MONSTER_TYPE = 'monster',
    EXIT_TYPE = 'exit',
    POWER_UP_BOMB_TYPE = 'powerup_bomb',
    POWER_UP_BLAST_TYPE = 'powerup_blast',
    PLAYER_TYPE = 'player',
}

// --- TEXT STYLES ---
export const TEXT_STYLES = {
    fontFamily: 'Arial',
    fontSize: 24,
    fill: "white",
    fontWeight: 'normal',
    stroke: 'black',
    strokeThickness: 2
};

// --- TEXTURES ---
export const TEXTURE_TABLE: string = "table";
export const TEXTURE_TABLE_DOWN: string = "table_down_";
export const TEXTURE_TABLE_UP: string = "table_upt_";
export const TEXTURE_TABLE_LEFT: string = "table_left_";
export const TEXTURE_TABLE_RIGHT: string = "table_right_";
export const TEXTURE_TABLE_CORNER_LEFT: string = "table_up_left_";
export const TEXTURE_TABLE_CORNER_RIGHT: string = "table_up_right_";

export const TEXTURE_BACKGROUND: string = "background_";
export const TEXTURE_BRICK: string = "bricks_";
export const TEXTURE_WALL: string = "wall_";
export const TEXTURE_BOMB: string = "bomb_";

export const enum PLAYER_TEXTURES {
    FORW_TEXTURE = 'forw_', 
    LEFT_TEXTURE = 'left_', 
    RIGHT_TEXTURE = 'right_', 
    BACK_TEXTURE = 'back_'
}

export const enum EXPLOSION_TEXTURES {
    CENTER_TEXTURE = 'center_',
    TOP_TEXTURE = 'top_', 
    LEFT_TEXTURE = 'left_', 
    BOTTOM_TEXTURE = 'bottom_', 
    RIGHT_TEXTURE = 'right_'
}

export const enum OBJECT_FACTORY {
    CREATE = "object_factory_create",
    DESTROY = "object_factory_destroy",
    REPLACE = "object_factory_replace"
}

export const enum ANIMATION_ACTIONS {
    ANIMATE = "animate",
    STOP_ANIMATE = "stop_animate",
    STOP_ANIMATE_AND_REPLACE = "stop_animate_and_destroy"
}

export const enum MONSTER_ANIMATION_ACTIONS {
    ANIMATE = "animate_monster",
    STOP_ANIMATE = "stop_animate_monster",
    STOP_ANIMATE_AND_REPLACE = "stop_animate_and_destroy_monster"
}

export const ANIMATION_BOMB_POWERUP = ["powerup_1", "powerup_2"];
export const ANIMATION_BLAST_POWERUP = ["blast_1", "blast_2"];
export const ANIMATION_EXIT = ["exit_1", "exit_2"];
export const ANIMATION_MONSTERS = {
    1: { "live": ["aaa_1", "aaa_2", "aaa_3"], "dead": ["aaa_4", "aaa_5", "aaa_6"] },
    2: { "live": ["bbb_1", "bbb_2", "bbb_3"], "dead": ["bbb_4", "bbb_5", "bbb_6"] },
    3: { "live": ["ccc_1", "ccc_2", "ccc_3"], "dead": ["ccc_4", "ccc_5", "ccc_6"] },
    4: { "live": ["ddd_1", "ddd_2", "ddd_3"], "dead": ["ddd_4", "ddd_5", "ddd_6"] },
} 

export const GAME_MAPS = {
    "game_level_1": {
        "walls": [
            {"x":0, "y":6},{"x":0, "y":10},{"x":0, "y":11},{"x":0, "y":12},{"x":1, "y":0},
            {"x":1, "y":10},{"x":1, "y":12},{"x":2, "y":0},{"x":2, "y":1},{"x":2, "y":3},{"x":2, "y":4},{"x":2, "y":12},
            {"x":3, "y":8},{"x":3, "y":10},{"x":4, "y":0},{"x":4, "y":3},{"x":4, "y":7},{"x":4, "y":8},
            {"x":4, "y":9},{"x":4, "y":11},{"x":4, "y":12},{"x":5, "y":2},{"x":5, "y":4},
            {"x":5, "y":12},{"x":6, "y":0},{"x":6, "y":7},{"x":6, "y":8},{"x":6, "y":9},{"x":6, "y":12},
            {"x":7, "y":0},{"x":7, "y":8},{"x":8, "y":1},{"x":8, "y":3},{"x":8, "y":5},
            {"x":8, "y":9},{"x":8, "y":11},{"x":9, "y":0},{"x":9, "y":2},{"x":9, "y":4},{"x":9, "y":6},
            {"x":9, "y":8},{"x":9, "y":12},{"x":10, "y":6},{"x":10, "y":9},{"x":10, "y":10},{"x":10, "y":11},
            {"x":11, "y":2},{"x":11, "y":4},{"x":11, "y":8},{"x":11, "y":10},{"x":12, "y":0},
            {"x":12, "y":3},{"x":12, "y":4},{"x":12, "y":5},{"x":12, "y":7},{"x":12, "y":10} 
        ],
        "players": {1:{"posX":0, "posY":0}, 2:{"posX":12, "posY":12}},
        "monsters": [
            {"type": 1, "posX":6, "posY":4}, {"type": 1, "posX":6, "posY":5}, {"type": 1, "posX":12, "posY":6}, 
            {"type": 3, "posX":2, "posY":10}, {"type": 3, "posX":6, "posY":10}, {"type": 3, "posX":10, "posY":0}
        ],
        "exit": {"x":6, "y":7},
        "powerUp": {"x": 2, "y": 0}
    },
    "game_level_2": {
        "walls": [
            {"x":0, "y":4},{"x":0, "y":5},{"x":0, "y":6},{"x":0, "y":7},{"x":0, "y":8},{"x":0, "y":12},
            {"x":1, "y":2},{"x":1, "y":10},{"x":1, "y":12},{"x":2, "y":0},{"x":2, "y":1},{"x":2, "y":5},
            {"x":2, "y":7},{"x":2, "y":9},{"x":2, "y":10},{"x":3, "y":4},{"x":3, "y":6},{"x":3, "y":8},
            {"x":4, "y":0},{"x":4, "y":1},{"x":4, "y":3},{"x":4, "y":4},{"x":4, "y":8},{"x":4, "y":11},
            {"x":5, "y":2},{"x":5, "y":4},{"x":5, "y":8},{"x":6, "y":3},{"x":6, "y":5},{"x":6, "y":6},
            {"x":6, "y":7},{"x":6, "y":8},{"x":6, "y":10},{"x":6, "y":11},{"x":6, "y":12},{"x":7, "y":0},
            {"x":7, "y":2},{"x":7, "y":8},{"x":7, "y":10},{"x":8, "y":0},{"x":8, "y":1},{"x":8, "y":4},
            {"x":8, "y":7},{"x":8, "y":10},{"x":8, "y":11},{"x":8, "y":12},{"x":9, "y":0},{"x":9, "y":2},{"x":9, "y":4},
            {"x":9, "y":10},{"x":10, "y":0},{"x":10, "y":1},{"x":10, "y":9},{"x":10, "y":11},{"x":11, "y":8},{"x":12, "y":0},
            {"x":12, "y":2}, {"x":12, "y":9}  
        ],
        "players": {1:{"posX":0, "posY":0}, 2:{"posX":12, "posY":12}},
        "monsters": [
            {"type": 4, "posX":6, "posY":4}, {"type": 4, "posX":6, "posY":2}, {"type": 4, "posX":1, "posY":6},
            {"type": 2, "posX":12, "posY":6}, {"type": 2, "posX":2, "posY":11}, {"type": 2, "posX":3, "posY":10}
        ],
        "exit": {"x":6, "y":7},
        "powerUp": {"x": 2, "y": 0}
    },
}

// --- MAP GENERATOR ---
/*let str = '';
for (let i = 0; i < 13; i++) {
    for (let j = 0; j < 13; j++) {
        if (i % 2 == 1 && j % 2 == 1) continue;
		if (Math.random() >= 0.6) {
			x = '{"x":' + i + ', "y":' + j + '},';
			str += x;
        }
    }
 }
console.log(str); */
