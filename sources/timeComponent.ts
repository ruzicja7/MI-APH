import { PLAYER_1_INPUTS, PLAYER_2_INPUTS, SOUNDS } from './constants';
import Component from '../ts/engine/Component';
import Scene from '../ts/engine/Scene';
import Msg from '../ts/engine/Msg';
import PIXIObjectBuilder from '../ts/engine/PIXIObjectBuilder';
import { PIXICmp } from '../ts/engine/PIXIObject';

export class TimeComponent extends Component {
    IS_PAUSED: boolean = true;
    REFRESH_SPEED: number = 1000; //ms
    LEVEL_TIMER: number = 300; //s
    private elapsed: number = 0;

    constructor(scene: Scene) {
        super(); 
        this.scene = scene;

        this.subscribe(PLAYER_1_INPUTS.PAUSE);
        this.subscribe(PLAYER_1_INPUTS.MOVING_UP);
        this.subscribe(PLAYER_1_INPUTS.MOVING_LEFT);
        this.subscribe(PLAYER_1_INPUTS.MOVING_RIGHT);
        this.subscribe(PLAYER_1_INPUTS.MOVING_DOWN);
        this.subscribe(PLAYER_2_INPUTS.MOVING_UP);
        this.subscribe(PLAYER_2_INPUTS.MOVING_LEFT);
        this.subscribe(PLAYER_2_INPUTS.MOVING_RIGHT);
        this.subscribe(PLAYER_2_INPUTS.MOVING_DOWN);
    }

    onMessage(msg: Msg) {
        if (msg.action === PLAYER_1_INPUTS.PAUSE) {
            this.IS_PAUSED = !this.IS_PAUSED;
        } else {
            // Start the game on the first player input
            this.IS_PAUSED = false;
        }
    }
    
    onInit() {
        let textObj = new PIXICmp.Text('stats_timer', '5:00');
        new PIXIObjectBuilder(this.scene).globalPos(267, 23).build(textObj);
        this.scene.stage.getPixiObj().addChild(textObj);
    }

    onUpdate(delta: number, absolute: number) {
        if (this.IS_PAUSED) return;

        // 1s refresh rate
        if (absolute - this.elapsed > this.REFRESH_SPEED) {
            this.elapsed = absolute;

            // Timer -1s
            let timerObj = <PIXICmp.Text> this.scene.findAllObjectsByTag('stats_timer')[0];
            timerObj.setText(this.calculateTimer());
        }
    }

    private calculateTimer(): string {
        if (this.LEVEL_TIMER > 0) this.LEVEL_TIMER --;
        let S = '';
        let M = '';

        if (this.LEVEL_TIMER == 0) {
            this.endGame();
            return;
        }

        M = (Math.floor(this.LEVEL_TIMER / 60)).toString();
        let seconds = this.LEVEL_TIMER % 60;

        if (seconds < 10) {
            S = '0' + seconds.toString();
        } else {
            S = seconds.toString();
        }

        return M + ':' + S;
    }

    private endGame() {
    	this.sendMessage(SOUNDS.LOOSE); // -> soundComponent
        this.scene.clearScene();
        let texture = PIXI.Texture.fromImage("menu_4");
        let menukObj = new PIXICmp.Sprite("menu_4", texture);
        menukObj.height = 825;
        menukObj.width = 750;
        new PIXIObjectBuilder(this.scene).globalPos(0, 0).build(menukObj);
        this.scene.stage.getPixiObj().addChild(menukObj);
    }
}