import { PLAYER_COLLISIONS, GAME_MAPS, TEXTURE_SIZE, TEXTURE_OFFSET_LEFT, TEXTURE_OFFSET_TOP } from './constants';
import Component from '../ts/engine/Component';
import Scene from '../ts/engine/Scene';
import Msg from '../ts/engine/Msg';
import { PIXICmp } from '../ts/engine/PIXIObject';

export class DieComponent extends Component {
    private playerId: number;
    private GAME_LEVEL: number;

    constructor(scene: Scene, playerId: number) {
        super();
        this.scene = scene;
        this.playerId = playerId;
        this.GAME_LEVEL = scene.getGlobalAttribute("level");

        this.subscribe(PLAYER_COLLISIONS.DIE);
    }

    onMessage(msg: Msg) {
        // resets P on starting position
        if (msg.data.playerId == this.playerId) {
           
            let gameLevel = GAME_MAPS['game_level_' + this.GAME_LEVEL];
            this.owner.getPixiObj().position.x = this.calculateStartPositionX();
            this.owner.getPixiObj().position.y = this.calculateStartPositionY();
        }
    }

    private calculateStartPositionX() {
        let posX = GAME_MAPS["game_level_" + this.GAME_LEVEL].players[this.playerId].posX;
        return posX * TEXTURE_SIZE + TEXTURE_OFFSET_LEFT;
    }

    private calculateStartPositionY() {
        let posY = GAME_MAPS["game_level_" + this.GAME_LEVEL].players[this.playerId].posY;
        return posY * TEXTURE_SIZE + (TEXTURE_OFFSET_TOP - 8);
    }

}