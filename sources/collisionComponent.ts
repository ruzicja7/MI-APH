import { PLAYER_1_INPUTS, PLAYER_2_INPUTS, PLAYER_COLLISIONS, GAME_TYPES, ANIMATION_ACTIONS, OBJECT_FACTORY, TEXTURE_BACKGROUND, PLAYER_STATS, SOUNDS } from './constants';
import { TEXTURE_SIZE, GAME_WIDTH, GAME_HEIGHT, TEXTURE_OFFSET_LEFT, TEXTURE_OFFSET_TOP } from './constants';
import Component from '../ts/engine/Component';
import { GameObject } from './gameObject';
import Scene from '../ts/engine/Scene';
import Msg from '../ts/engine/Msg';
import { PIXICmp } from '../ts/engine/PIXIObject';
import PIXIObjectBuilder from '../ts/engine/PIXIObjectBuilder';

export class CollisionComponent extends Component {
    private posX: number = 0;
    private posY: number = 0;
    ALLOWED_OFFSET: number = 3;
    GAME_LEVEL: number = null; 
    private actual_move: string = 'none';

    private game_map: Array<Array<GameObject>> = null; 
    private bomb_map: Array<Array<GameObject>> = null; 
    private monster_map: Array<Array<GameObject>> = null; 
    private playerId: number;

    constructor(scene: Scene, game_map: Array<Array<GameObject>>, bomb_map: Array<Array<GameObject>>, monster_map: Array<Array<GameObject>>, playerId: number) {
        super();
        this.scene = scene;
        this.game_map = game_map;
        this.bomb_map = bomb_map;
        this.monster_map = monster_map;
        this.playerId = playerId;
        this.GAME_LEVEL = scene.getGlobalAttribute("level");

        this.subscribe(PLAYER_1_INPUTS.MOVING_UP);
        this.subscribe(PLAYER_1_INPUTS.MOVING_RIGHT);
        this.subscribe(PLAYER_1_INPUTS.MOVING_DOWN);
        this.subscribe(PLAYER_1_INPUTS.MOVING_LEFT);
        this.subscribe(PLAYER_2_INPUTS.MOVING_UP);
        this.subscribe(PLAYER_2_INPUTS.MOVING_RIGHT);
        this.subscribe(PLAYER_2_INPUTS.MOVING_DOWN);
        this.subscribe(PLAYER_2_INPUTS.MOVING_LEFT);
    }

    onMessage(msg: Msg) {
        this.posX = msg.data.posX;
        this.posY = msg.data.posY;
        this.actual_move = msg.action;

        if (msg.data.playerId == this.playerId) {
            this.checkCollision();
        }
    }

    private checkOverLeaping(playerBounds: any, objectBounds: any): boolean {
        let plus = Math.pow((playerBounds.x - objectBounds.x), 2) + Math.pow((playerBounds.y - objectBounds.y), 2);
        let distance = Math.sqrt(plus);

        // Collision
        if (distance < TEXTURE_SIZE - this.ALLOWED_OFFSET) {
            //console.log(playerBounds, objectBounds);

            // Bomb drop under feet 
            if (distance < 45 && objectBounds.objectType === GAME_TYPES.BOMB_TYPE) return;

            if (playerBounds.x - objectBounds.x > 0) { 
                //console.log('<-'); 
                this.sendMessage(PLAYER_COLLISIONS.LEFT, { playerId: this.playerId }); // -> moveComponent
                return true;
            }
            if (playerBounds.x - objectBounds.x < 0) {
                //console.log('->');
                this.sendMessage(PLAYER_COLLISIONS.RIGHT, { playerId: this.playerId }); // -> moveComponent
                return true;
            }
            if (playerBounds.y - objectBounds.y > 0) {
                //console.log('^');
                this.sendMessage(PLAYER_COLLISIONS.TOP, { playerId: this.playerId }); // -> moveComponent
                return true;
            }
            if (playerBounds.y - objectBounds.y < 0) {
                //console.log('v');
                this.sendMessage(PLAYER_COLLISIONS.BOTTOM, { playerId: this.playerId }); // -> moveComponent
                return true;
            }
        }

        return false;
    }

    private checkCollision() {
        // Check borders collisions
        let absoluteX = Math.round(this.posX - TEXTURE_OFFSET_LEFT);
        let absoluteY = Math.round(this.posY - (TEXTURE_OFFSET_TOP - 8));

        if (absoluteX + this.ALLOWED_OFFSET <= 0 ) 
            this.sendMessage(PLAYER_COLLISIONS.LEFT, { playerId: this.playerId }); // -> moveComponent
        if (absoluteX - this.ALLOWED_OFFSET >= (GAME_WIDTH - 1) * TEXTURE_SIZE) 
            this.sendMessage(PLAYER_COLLISIONS.RIGHT, { playerId: this.playerId }); // -> moveComponent
        if (absoluteY + this.ALLOWED_OFFSET <= 0 ) 
            this.sendMessage(PLAYER_COLLISIONS.TOP, { playerId: this.playerId }); // -> moveComponent
        if (absoluteY - this.ALLOWED_OFFSET >= (GAME_HEIGHT - 1) * TEXTURE_SIZE) 
            this.sendMessage(PLAYER_COLLISIONS.BOTTOM, { playerId: this.playerId }); // -> moveComponent

        // Check objects collisions
        let mapX = Math.round((this.posX - TEXTURE_OFFSET_LEFT) / TEXTURE_SIZE);
        let mapY = Math.round(((this.posY - (TEXTURE_OFFSET_TOP - 8)) / TEXTURE_SIZE));
        let playerCenter = { x: absoluteX, y: absoluteY, objectType: GAME_TYPES.PLAYER_TYPE };
        for (let i = 0; i < GAME_WIDTH; i ++) {
            for (let j = 0; j < GAME_HEIGHT; j ++) {

                // Exit
                if (this.game_map[i][j].getGameType() == GAME_TYPES.EXIT_TYPE) {
                    let absoluteObjX = i * TEXTURE_SIZE;
                    let absoluteObjY = j * TEXTURE_SIZE;

                    let objCenter = { x: absoluteObjX, y: absoluteObjY, objectType: this.game_map[i][j].getGameType() };
                    if (this.checkOverLeaping(playerCenter, objCenter)) {
                        this.scene.clearScene();
                        this.sendMessage(SOUNDS.WIN); // -> soundComponent
                        let texture = PIXI.Texture.fromImage("menu_3");
                        let menukObj = new PIXICmp.Sprite("menu_3", texture);
                        menukObj.height = 825;
                        menukObj.width = 750;
                        new PIXIObjectBuilder(this.scene).globalPos(0, 0).build(menukObj);
                        this.scene.stage.getPixiObj().addChild(menukObj);
                    }
                }

                // PowerUp
                if (this.game_map[i][j].getGameType() == GAME_TYPES.POWER_UP_BOMB_TYPE) {
                    let absoluteObjX = i * TEXTURE_SIZE;
                    let absoluteObjY = j * TEXTURE_SIZE;

                    let objCenter = { x: absoluteObjX, y: absoluteObjY, objectType: this.game_map[i][j].getGameType() };
                    if (this.checkOverLeaping(playerCenter, objCenter)) {
                        let data = { type: GAME_TYPES.BACKGROUND_TYPE, position: { posX: i, posY: j }, textureTag: TEXTURE_BACKGROUND + this.GAME_LEVEL }; 
                        this.sendMessage(ANIMATION_ACTIONS.STOP_ANIMATE, { "spriteId": this.game_map[i][j].getPixiSprite().getId() }); // -> textureAnimationComponent
                        this.sendMessage(OBJECT_FACTORY.REPLACE, data); // -> objectFactoryComponent
                        this.sendMessage(PLAYER_STATS.BOMB_MAX_COUNT_PLUS); // -> bombComponent
                        this.sendMessage(PLAYER_COLLISIONS.RESET, { "playerId": "x" } ); // -> collisionComponent
                        this.sendMessage(SOUNDS.POWERUP); // -> soundComponent
                    }
                }

                // Only one wallkable object
                if (this.game_map[i][j].getGameType() != GAME_TYPES.BACKGROUND_TYPE) {
                    let absoluteObjX = i * TEXTURE_SIZE;
                    let absoluteObjY = j * TEXTURE_SIZE;

                    let objCenter = { x: absoluteObjX, y: absoluteObjY, objectType: this.game_map[i][j].getGameType() };
                    this.checkOverLeaping(playerCenter, objCenter);
                }
                
                // Bomb objects
                if (this.bomb_map[i][j]) {
                    let absoluteObjX = i * TEXTURE_SIZE;
                    let absoluteObjY = j * TEXTURE_SIZE;

                    let objCenter = { x: absoluteObjX, y: absoluteObjY, objectType: this.bomb_map[i][j].getGameType() };
                    this.checkOverLeaping(playerCenter, objCenter);
                }
            } 
        }
    }
}