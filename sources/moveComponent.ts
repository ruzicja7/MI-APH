import { PLAYER_1_INPUTS, PLAYER_2_INPUTS, PLAYER_COLLISIONS, TEXTURE_OFFSET_LEFT } from './constants';
import { TEXTURE_SIZE } from './constants';
import Component from '../ts/engine/Component';
import Scene from '../ts/engine/Scene';
import Msg from '../ts/engine/Msg';

export class MoveComponent extends Component {
    MOVE_SPEED: number = 0.15;
    playerId: number;
    ALLOWED_OFFSET: number = 2;
    private keys_pressed = new Set<string>();
    private collision_keys = new Set<string>(); 

    constructor(scene: Scene, playerId: number) {
        super();
        this.scene = scene;
        this.playerId = playerId;

        document.addEventListener('keydown', this.onKeyDown.bind(this)); // bind twice..
        document.addEventListener('keyup', this.onKeyUp.bind(this));

        this.subscribe(PLAYER_COLLISIONS.TOP);
        this.subscribe(PLAYER_COLLISIONS.RIGHT);
        this.subscribe(PLAYER_COLLISIONS.BOTTOM);
        this.subscribe(PLAYER_COLLISIONS.LEFT);
        this.subscribe(PLAYER_COLLISIONS.RESET);        
    }

    onMessage(msg: Msg) {
        if (msg.data.playerId == 1) {
            if (msg.action === PLAYER_COLLISIONS.TOP) {
                this.keys_pressed.delete('ArrowUp');
                this.collision_keys.add('ArrowUp');
            }
            if (msg.action === PLAYER_COLLISIONS.RIGHT) {
                this.keys_pressed.delete('ArrowRight');
                this.collision_keys.add('ArrowRight');
            }
            if (msg.action === PLAYER_COLLISIONS.BOTTOM) {
                this.keys_pressed.delete('ArrowDown');
                this.collision_keys.add('ArrowDown');
            }
            if (msg.action === PLAYER_COLLISIONS.LEFT) {
                this.keys_pressed.delete('ArrowLeft');
                this.collision_keys.add('ArrowLeft');
            }
        }

        if (msg.data.playerId == 2) {
            if (msg.action === PLAYER_COLLISIONS.TOP) {
                this.keys_pressed.delete('KeyW');
                this.collision_keys.add('KeyW');
            }
            if (msg.action === PLAYER_COLLISIONS.RIGHT) {
                this.keys_pressed.delete('KeyD');
                this.collision_keys.add('KeyD');
            }
            if (msg.action === PLAYER_COLLISIONS.BOTTOM) {
                this.keys_pressed.delete('KeyS');
                this.collision_keys.add('KeyS');
            }
            if (msg.action === PLAYER_COLLISIONS.LEFT) {
                this.keys_pressed.delete('KeyA');
                this.collision_keys.add('KeyA');
            }
        }

        if (msg.action === PLAYER_COLLISIONS.RESET) {
            this.collision_keys.clear();
        }
    }

    // Key Handlers
    onKeyDown(key: KeyboardEvent) {
        if (this.isInCollision(key.code)) return;
        
        if (this.playerId == 1) 
            this.allowKeysP1(key.code); 
        if (this.playerId == 2) 
            this.allowKeysP2(key.code); 
    
        this.keys_pressed.add(key.code);
    }

    onKeyUp(key: KeyboardEvent) {
        if (this.playerId == 1) 
            this.allowKeysP1(key.code); 
        if (this.playerId == 2) 
            this.allowKeysP2(key.code); 
        
        this.keys_pressed.delete(key.code);
    }

    onUpdate(delta: number, absolute: number) {
        if (this.keys_pressed.size != 0) {
            if (this.playerId == 1)
                this.updateP1(delta, absolute);
            if (this.playerId == 2)   
                this.updateP2(delta, absolute);
        } else {
            if (this.playerId == 1)
                this.sendMessage(PLAYER_1_INPUTS.EMPTY, { playerId: this.playerId }); // -> playerTextureComponent
            if (this.playerId == 2)    
                this.sendMessage(PLAYER_2_INPUTS.EMPTY, { playerId: this.playerId }); // -> playerTextureComponent
        }

        if (this.isKeyPressed('Enter')) {
            this.sendMessage(PLAYER_1_INPUTS.PAUSE);
        }
    }

    private updateP1(delta: number, absolute: number) {
        if (this.isKeyPressed('ArrowUp')) {
            this.owner.getPixiObj().position.y -= delta * this.MOVE_SPEED;
            this.adjustX();

            let position = { playerId: this.playerId, posX: this.owner.getPixiObj().position.x, posY: this.owner.getPixiObj().position.y };
            this.sendMessage(PLAYER_1_INPUTS.MOVING_UP, position); // -> playerTextureComponent, collisionComponent, monsterMoveComponent
        } 
        if (this.isKeyPressed('ArrowLeft')) {
            this.owner.getPixiObj().position.x -= delta * this.MOVE_SPEED;
            this.adjustY();

            let position = { playerId: this.playerId, posX: this.owner.getPixiObj().position.x, posY: this.owner.getPixiObj().position.y };
            this.sendMessage(PLAYER_1_INPUTS.MOVING_LEFT, position); // -> playerTextureComponent, collisionComponent, monsterMoveComponent
        }
        if (this.isKeyPressed('ArrowRight')) {
            this.owner.getPixiObj().position.x += delta * this.MOVE_SPEED;
            this.adjustY();

            let position = { playerId: this.playerId, posX: this.owner.getPixiObj().position.x, posY: this.owner.getPixiObj().position.y };
            this.sendMessage(PLAYER_1_INPUTS.MOVING_RIGHT, position); // -> playerTextureComponent, collisionComponent, monsterMoveComponent
        }
        if (this.isKeyPressed('ArrowDown')) {
            this.owner.getPixiObj().position.y += delta * this.MOVE_SPEED;
            this.adjustX();

            let position = { playerId: this.playerId, posX: this.owner.getPixiObj().position.x, posY: this.owner.getPixiObj().position.y };
            this.sendMessage(PLAYER_1_INPUTS.MOVING_DOWN, position); // -> playerTextureComponent, collisionComponent, monsterMoveComponent
        }
        if (this.isKeyPressed('Space')) {
            let position = { playerId: this.playerId, posX: this.owner.getPixiObj().position.x, posY: this.owner.getPixiObj().position.y };
            this.sendMessage(PLAYER_1_INPUTS.DROP_BOMB, position); // -> bombComponent
        }
    }

    private updateP2(delta: number, absolute: number) {
        if (this.isKeyPressed('KeyW')) {
            this.owner.getPixiObj().position.y -= delta * this.MOVE_SPEED;
            this.adjustX();

            let position = { playerId: this.playerId, posX: this.owner.getPixiObj().position.x, posY: this.owner.getPixiObj().position.y };
            this.sendMessage(PLAYER_2_INPUTS.MOVING_UP, position); // -> playerTextureComponent, collisionComponent, monsterMoveComponent
        } 
        if (this.isKeyPressed('KeyA')) {
            this.owner.getPixiObj().position.x -= delta * this.MOVE_SPEED;
            this.adjustY();

            let position = { playerId: this.playerId, posX: this.owner.getPixiObj().position.x, posY: this.owner.getPixiObj().position.y };
            this.sendMessage(PLAYER_2_INPUTS.MOVING_LEFT, position); // -> playerTextureComponent, collisionComponent, monsterMoveComponent
        }
        if (this.isKeyPressed('KeyD')) {
            this.owner.getPixiObj().position.x += delta * this.MOVE_SPEED;
            this.adjustY();

            let position = { playerId: this.playerId, posX: this.owner.getPixiObj().position.x, posY: this.owner.getPixiObj().position.y };
            this.sendMessage(PLAYER_2_INPUTS.MOVING_RIGHT, position); // -> playerTextureComponent, collisionComponent, monsterMoveComponent
        }
        if (this.isKeyPressed('KeyS')) {
            this.owner.getPixiObj().position.y += delta * this.MOVE_SPEED;
            this.adjustX();

            let position = { playerId: this.playerId, posX: this.owner.getPixiObj().position.x, posY: this.owner.getPixiObj().position.y };
            this.sendMessage(PLAYER_2_INPUTS.MOVING_DOWN, position); // -> playerTextureComponent, collisionComponent, monsterMoveComponent
        }
        if (this.isKeyPressed('ControlLeft')) {
            let position = { playerId: this.playerId, posX: this.owner.getPixiObj().position.x, posY: this.owner.getPixiObj().position.y };
            this.sendMessage(PLAYER_2_INPUTS.DROP_BOMB, position); // -> bombComponent
        }
    }

    // Adjusts players [x, y]
    private adjustX() {
        this.owner.getPixiObj().position.x = (Math.round((this.owner.getPixiObj().position.x - TEXTURE_OFFSET_LEFT) / TEXTURE_SIZE) * TEXTURE_SIZE) + TEXTURE_OFFSET_LEFT;
    }

    private adjustY() {
        this.owner.getPixiObj().position.y = (Math.round((this.owner.getPixiObj().position.y) / TEXTURE_SIZE) * TEXTURE_SIZE) - 8;
    }

    private isKeyPressed(keyCode: string) {
        return this.keys_pressed.has(keyCode) 
    }

    private isInCollision(keyCode: string) {
        return this.collision_keys.has(keyCode)
    }

    private allowKeysP1(key: string) {
        if (key === 'ArrowUp') {
            this.collision_keys.delete('ArrowDown');
            this.collision_keys.delete('ArrowLeft');
            this.collision_keys.delete('ArrowRight');
        }
        if (key === 'ArrowRight') {
            this.collision_keys.delete('ArrowLeft');
            this.collision_keys.delete('ArrowUp');
            this.collision_keys.delete('ArrowDown');
        }
        if (key === 'ArrowDown') {
            this.collision_keys.delete('ArrowUp');
            this.collision_keys.delete('ArrowLeft');
            this.collision_keys.delete('ArrowRight');
        }
        if (key === 'ArrowLeft') {
            this.collision_keys.delete('ArrowRight');
            this.collision_keys.delete('ArrowUp');
            this.collision_keys.delete('ArrowDown');
        }
    }

    private allowKeysP2(key: string) {
        if (key === 'KeyW') {
            this.collision_keys.delete('KeyS');
            this.collision_keys.delete('KeyD');
            this.collision_keys.delete('KeyA');
        }
        if (key === 'KeyD') {
            this.collision_keys.delete('KeyA');
            this.collision_keys.delete('KeyS');
            this.collision_keys.delete('KeyW');
        }
        if (key === 'KeyS') {
            this.collision_keys.delete('KeyW');
            this.collision_keys.delete('KeyA');
            this.collision_keys.delete('KeyD');
        }
        if (key === 'KeyA') {
            this.collision_keys.delete('KeyD');
            this.collision_keys.delete('KeyW');
            this.collision_keys.delete('KeyS');
        }
    }
}