import Scene from '../ts/engine/Scene';
import { PLAYER_TEXTURES, GAME_WIDTH, GAME_MAPS } from './constants';
import { TEXTURE_OFFSET_LEFT, TEXTURE_OFFSET_TOP, TEXTURE_SIZE } from './constants';
import { PIXICmp } from '../ts/engine/PIXIObject';
import PIXIObjectBuilder from '../ts/engine/PIXIObjectBuilder';
import { GameObject } from './gameObject';
import { MoveComponent } from './moveComponent'
import { PlayerTextureComponent } from './playerTextureComponent'
import { CollisionComponent } from './collisionComponent'
import { BombComponent } from './bombComponent'
import { DieComponent } from './dieComponent'

export class Player {
    private game_map: Array<Array<GameObject>> = null;
    private bomb_map: Array<Array<GameObject>> = null;
    private monster_map: Array<Array<GameObject>> = null;
    private playerId: number;

    GAME_LEVEL: number = null;

    constructor(game_map: Array<Array<GameObject>>, bomb_map: Array<Array<GameObject>>, monster_map: Array<Array<GameObject>>, playerId: number) {
        this.game_map = game_map;
        this.bomb_map = bomb_map;
        this.monster_map = monster_map;
        this.playerId = playerId;
    }

    init(scene: Scene) {
        this.GAME_LEVEL = scene.getGlobalAttribute("level");

        let playerTexture = PLAYER_TEXTURES.FORW_TEXTURE + 1;
        let texture = PIXI.Texture.fromImage(playerTexture);

        let playerObj = new PIXICmp.Sprite(playerTexture, texture);
        if (this.playerId == 2) {
            playerObj.tint = 0x333333;
        }
        
        new PIXIObjectBuilder(scene).globalPos(this.calculateStartPositionX(), this.calculateStartPositionY()).anchor(0.5, 0.5).build(playerObj);
        scene.stage.getPixiObj().addChild(playerObj); 
        
        playerObj.addComponent(new MoveComponent(scene, this.playerId));
        playerObj.addComponent(new PlayerTextureComponent(scene, this.playerId)); 
        playerObj.addComponent(new CollisionComponent(scene, this.game_map, this.bomb_map, this.monster_map, this.playerId));  
        playerObj.addComponent(new BombComponent(scene, this.game_map, this.bomb_map, this.monster_map, this.playerId)); 
        playerObj.addComponent(new DieComponent(scene, this.playerId)); 
    }

    private calculateStartPositionX() {
        let posX = GAME_MAPS["game_level_" + this.GAME_LEVEL].players[this.playerId].posX;
        return posX * TEXTURE_SIZE + TEXTURE_OFFSET_LEFT;
    }

    private calculateStartPositionY() {
        let posY = GAME_MAPS["game_level_" + this.GAME_LEVEL].players[this.playerId].posY;
        return posY * TEXTURE_SIZE + (TEXTURE_OFFSET_TOP - 8);
    }
}