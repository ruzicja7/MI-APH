import { ANIMATION_ACTIONS, GAME_TYPES, ANIMATION_EXIT, ANIMATION_BOMB_POWERUP, ANIMATION_BLAST_POWERUP } from './constants';
import Component from '../ts/engine/Component';
import Scene from '../ts/engine/Scene';
import Msg from '../ts/engine/Msg';
import { GameObject } from './gameObject';
import { PIXICmp } from '../ts/engine/PIXIObject';

export class TextureAnimationComponent extends Component {
    DEFAULT_ANIMATION_SPEED = 500; //ms

    private game_map: Array<Array<GameObject>>;
    private objectsToAnimate = [];

    constructor(scene: Scene, game_map: Array<Array<GameObject>>) {
        super();
        this.scene = scene; 
        this.game_map = game_map;

        this.subscribe(ANIMATION_ACTIONS.ANIMATE);
        this.subscribe(ANIMATION_ACTIONS.STOP_ANIMATE);
        this.subscribe(ANIMATION_ACTIONS.STOP_ANIMATE_AND_REPLACE);
    }

    onMessage(msg: Msg) {
        if (msg.action === ANIMATION_ACTIONS.ANIMATE) {
            this.animateObject(msg.data); 
        }
        if (msg.action === ANIMATION_ACTIONS.STOP_ANIMATE) {
            this.stopAnimation(msg.data);
        }
    }

    onUpdate(delta: number, absolute: number) {

        this.objectsToAnimate.forEach(obj => {
            if (obj) {
                if (obj.updated == 0) {
                    obj.updated = absolute;
                } else {
                    if (absolute - obj.updated > obj.animationSpeed) {
                        obj.updated = absolute;
                        
                        obj.textureIndex = this.getNextIndex(obj.textureIndex, obj.textures.length);
                        let texture = PIXI.Texture.fromImage(obj.textures[obj.textureIndex]);
                        this.game_map[obj.position.posX][obj.position.posY].getPixiSprite().texture = texture;
                    }
                }
            }
        });
    }

    private animateObject(data: any) {
        let posX = data.position.posX;
        let posY = data.position.posY;
        let animationSpeed = (data.animationSpeed) ? data.animationSpeed : this.DEFAULT_ANIMATION_SPEED;
        let textures = data.textures;

        let obj = { spriteId: data.spriteId, position: { posX: posX, posY: posY }, animationSpeed: animationSpeed, updated: 0, textures: textures, textureIndex: 0 };
        this.objectsToAnimate[data.spriteId] = obj;
    }
    
    private stopAnimation(data: any) {
        delete this.objectsToAnimate[data.spriteId];
    }

    private getNextIndex(index: number, arrayLength: number): number {
        return (index < arrayLength-1) ? index+1 : 0; 
    }
}